﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Circulo : Figura
    {
        public int Radio { get; set; }

        public Circulo(Color color) : base(color)
        {
        }

        public Circulo(Color color, int radio) : base(color)
        {
            Radio = radio;
        }

        public override void Pintar()
        {
            Console.WriteLine($"Pinta un círculo de color {ColorFigura}");
        }
    }
}
