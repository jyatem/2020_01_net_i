﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Figura> figuras = new List<Figura>();

            figuras.Add(new Figura(Color.Amarillo));
            figuras.Add(new Circulo(Color.Rojo));
            figuras.Add(new Linea(Color.Azul));
            figuras.Add(new Circulo(Color.Azul, 10));

            foreach (var figura in figuras)
            {
                figura.Pintar();
            }

            Console.ReadLine();
        }
    }
}
