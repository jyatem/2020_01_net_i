﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Linea : Figura
    {
        public Linea(Color color) : base(color)
        {
            
        }

        public int NombreFigura
        {
            get => default;
            set
            {
            }
        }

        public override void Pintar()
        {
            Console.WriteLine($"Pinta una línea de color {ColorFigura}");
        }
    }
}
