﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaCalculadora
{
    public class Calculadora
    {
        public int Sumar(int valor1, int valor2)
        {
            return valor1 + valor2;
        }

        // División
        // División de enteros 6/3 = 2
        // División numerador es 0 0/4 = 0
        // División denominador es 0 7/0 -> Excepción
        public int Dividir(int numerador, int denominador)
        {
            if (denominador == 0)
            {
                throw new DivideByZeroException("El deominador es cero y genera error");
            }
            
            return numerador / denominador;
        }
    }
}
