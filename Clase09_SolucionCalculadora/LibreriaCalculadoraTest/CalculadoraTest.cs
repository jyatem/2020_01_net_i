﻿using System;
using LibreriaCalculadora;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibreriaCalculadoraTest
{
    [TestClass]
    public class CalculadoraTest
    {
        [TestMethod]
        public void Sumar_Enteros()
        {
            // Arrange
            Calculadora calculadora = new Calculadora();

            // Act
            int resultado = calculadora.Sumar(2, 3);

            // Assert
            Assert.AreEqual(5, resultado);
        }

        [TestMethod]
        public void Sumar_Numeros_Negativos()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Sumar(-1, -6);

            Assert.AreEqual(-7, resultado);

        }

        [TestMethod]
        public void DividirEnteros()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Dividir(8, 4);

            Assert.AreEqual(2, resultado);
        }

        [TestMethod]
        public void DividirNumeradorCero()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Dividir(0, 4);

            Assert.AreEqual(0, resultado);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void DividirDenominadorCero()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Dividir(5, 0);
        }
    }
}
