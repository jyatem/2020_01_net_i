﻿namespace WF_Contenedor
{
    partial class FormOtroConUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucPaisDepartamento1 = new WF_Contenedor.UCPaisDepartamento();
            this.SuspendLayout();
            // 
            // ucPaisDepartamento1
            // 
            this.ucPaisDepartamento1.Location = new System.Drawing.Point(37, 12);
            this.ucPaisDepartamento1.Name = "ucPaisDepartamento1";
            this.ucPaisDepartamento1.Size = new System.Drawing.Size(273, 112);
            this.ucPaisDepartamento1.TabIndex = 0;
            // 
            // FormOtroConUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 282);
            this.Controls.Add(this.ucPaisDepartamento1);
            this.Name = "FormOtroConUserControl";
            this.Text = "FormOtroConUserControl";
            this.ResumeLayout(false);

        }

        #endregion

        private UCPaisDepartamento ucPaisDepartamento1;
    }
}