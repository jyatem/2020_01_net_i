﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class FormContenido : Form
    {
        public FormContenido()
        {
            InitializeComponent();
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (saveFileDialog.ShowDialog().Equals(DialogResult.OK))
                {
                    Stream stream = saveFileDialog.OpenFile();
                    StreamWriter streamWriter = new StreamWriter(stream);

                    foreach (string linea in txtContenido.Lines)
                    {
                        streamWriter.WriteLine(linea);
                    }

                    streamWriter.Close();
                    stream.Close();

                    MessageBox.Show("Se guardó exitosamente", "Contenido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
