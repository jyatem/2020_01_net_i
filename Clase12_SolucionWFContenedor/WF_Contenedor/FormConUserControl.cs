﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class FormConUserControl : Form
    {
        public FormConUserControl()
        {
            InitializeComponent();
        }

        private void btnSeleccionados_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"{ucPaisDepartamento.cmbCiudad.Text} del departamento {ucPaisDepartamento.cmbDepartamento.Text} del país {ucPaisDepartamento.cmbPais.Text}");
        }
    }
}
