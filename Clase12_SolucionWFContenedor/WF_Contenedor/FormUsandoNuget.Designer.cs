﻿namespace WF_Contenedor
{
    partial class FormUsandoNuget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtxtContenido = new System.Windows.Forms.RichTextBox();
            this.btnIdentificar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtxtContenido
            // 
            this.rtxtContenido.Location = new System.Drawing.Point(33, 32);
            this.rtxtContenido.Name = "rtxtContenido";
            this.rtxtContenido.Size = new System.Drawing.Size(282, 148);
            this.rtxtContenido.TabIndex = 0;
            this.rtxtContenido.Text = "";
            // 
            // btnIdentificar
            // 
            this.btnIdentificar.Location = new System.Drawing.Point(127, 186);
            this.btnIdentificar.Name = "btnIdentificar";
            this.btnIdentificar.Size = new System.Drawing.Size(75, 23);
            this.btnIdentificar.TabIndex = 1;
            this.btnIdentificar.Text = "Identificar";
            this.btnIdentificar.UseVisualStyleBackColor = true;
            this.btnIdentificar.Click += new System.EventHandler(this.btnIdentificar_Click);
            // 
            // FormUsandoNuget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 281);
            this.Controls.Add(this.btnIdentificar);
            this.Controls.Add(this.rtxtContenido);
            this.Name = "FormUsandoNuget";
            this.Text = "FormUsandoNuget";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtxtContenido;
        private System.Windows.Forms.Button btnIdentificar;
    }
}