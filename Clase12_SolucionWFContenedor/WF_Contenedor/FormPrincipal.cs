﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DialogResult dialogResult = MessageBox.Show("¿Realmente quiere cerrar la aplicación?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //e.Cancel = (dialogResult == DialogResult.No);
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAcercaDe formAcercaDe = new FormAcercaDe();
            formAcercaDe.ShowDialog();
        }

        private void contenidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirContenido();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AbrirContenido();
        }

        private void AbrirContenido()
        {
            FormContenido formContenido = new FormContenido();
            formContenido.MdiParent = this;
            formContenido.Show();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formularioActivo = this.ActiveMdiChild;

            if (formularioActivo != null)
            {
                formularioActivo.Close();
            }
        }

        // LayoutMdi

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void cascadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void detectarIdiomaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormUsandoNuget formUsandoNuget = new FormUsandoNuget();
            formUsandoNuget.ShowDialog();
        }

        private void sinUserControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSinUserControl formSinUserControl = new FormSinUserControl();
            formSinUserControl.ShowDialog();
        }

        private void conUserControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConUserControl formConUserControl = new FormConUserControl();
            formConUserControl.ShowDialog();
        }

        private void otroConUserControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormOtroConUserControl formOtroConUserControl = new FormOtroConUserControl();
            formOtroConUserControl.ShowDialog();
        }
    }
}
