﻿namespace WF_Contenedor
{
    partial class FormConUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucPaisDepartamento = new WF_Contenedor.UCPaisDepartamento();
            this.btnSeleccionados = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ucPaisDepartamento
            // 
            this.ucPaisDepartamento.Location = new System.Drawing.Point(27, 12);
            this.ucPaisDepartamento.Name = "ucPaisDepartamento";
            this.ucPaisDepartamento.Size = new System.Drawing.Size(273, 111);
            this.ucPaisDepartamento.TabIndex = 0;
            // 
            // btnSeleccionados
            // 
            this.btnSeleccionados.Location = new System.Drawing.Point(133, 129);
            this.btnSeleccionados.Name = "btnSeleccionados";
            this.btnSeleccionados.Size = new System.Drawing.Size(103, 23);
            this.btnSeleccionados.TabIndex = 1;
            this.btnSeleccionados.Text = "Seleccionados";
            this.btnSeleccionados.UseVisualStyleBackColor = true;
            this.btnSeleccionados.Click += new System.EventHandler(this.btnSeleccionados_Click);
            // 
            // FormConUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 305);
            this.Controls.Add(this.btnSeleccionados);
            this.Controls.Add(this.ucPaisDepartamento);
            this.Name = "FormConUserControl";
            this.Text = "FormConUserControl";
            this.ResumeLayout(false);

        }

        #endregion

        private UCPaisDepartamento ucPaisDepartamento;
        private System.Windows.Forms.Button btnSeleccionados;
    }
}