﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class FormSinUserControl : Form
    {
        public FormSinUserControl()
        {
            InitializeComponent();
        }

        private void cmbPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbDepartamento.Text = "";
            cmbDepartamento.Items.Clear();

            if (cmbPais.Text == "Colombia")
            {
                cmbDepartamento.Items.Add("Antioquia");
                cmbDepartamento.Items.Add("Valle del Cauca");
                cmbDepartamento.Items.Add("Santander");
            }
            else if (cmbPais.Text == "Ecuador")
            {
                cmbDepartamento.Items.Add("Pichincha");
                cmbDepartamento.Items.Add("Quito");
            }
        }
    }
}
