﻿using Allan.Language.Detection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class FormUsandoNuget : Form
    {
        public FormUsandoNuget()
        {
            InitializeComponent();
        }

        private void btnIdentificar_Click(object sender, EventArgs e)
        {
            var lenguajes = Detector.DetectLanguage(rtxtContenido.Text);

            var primerLenguaje = lenguajes.OrderBy(leng => leng.Distance).First();

            var lenguajeAsignado = new CultureInfo(primerLenguaje.Language.Code).DisplayName;

            MessageBox.Show(lenguajeAsignado);
        }
    }
}
