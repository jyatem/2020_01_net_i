﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class UCPaisDepartamento : UserControl
    {
        public UCPaisDepartamento()
        {
            InitializeComponent();
        }

        private void cmbPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbDepartamento.Text = "";
            cmbDepartamento.Items.Clear();

            if (cmbPais.Text == "Colombia")
            {
                cmbDepartamento.Items.Add("Antioquia");
                cmbDepartamento.Items.Add("Valle del Cauca");
                cmbDepartamento.Items.Add("Santander");
            }
            else if (cmbPais.Text == "Ecuador")
            {
                cmbDepartamento.Items.Add("Pichincha");
                cmbDepartamento.Items.Add("Quito");
            }
        }

        private void cmbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbCiudad.Text = "";
            cmbCiudad.Items.Clear();

            if (cmbDepartamento.Text == "Antioquia")
            {
                cmbCiudad.Items.Add("Medellin");
                cmbCiudad.Items.Add("Envigado");
                cmbCiudad.Items.Add("Itagui");
            }
            else if (cmbDepartamento.Text == "Valle del Cauca")
            {
                cmbCiudad.Items.Add("Cali");
                cmbCiudad.Items.Add("Buga");
                cmbCiudad.Items.Add("Tulua");
            }
        }
    }
}
