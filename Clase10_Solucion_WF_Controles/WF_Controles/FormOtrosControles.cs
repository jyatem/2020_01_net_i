﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Controles
{
    public partial class FormOtrosControles : Form
    {
        public FormOtrosControles()
        {
            InitializeComponent();
        }

        private void FormOtrosControles_Load(object sender, EventArgs e)
        {
            btnIzquierdo.Text = "Izquierdo";
            btnDerecho.Text = "Derecho";
            btnCentro.Text = "Centro";
            btnSuperior.Text = "Superior";

            pbBarraProgreso.Maximum = 100;

            toolTip.SetToolTip(btnEjecutar, "Este botón es para ejecutar un proceso largo");
            toolTip.SetToolTip(btnSuperior, "Este el el botón superior");
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {   
            btnEjecutar.Enabled = false;

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(100);
                pbBarraProgreso.Value = i;
            }

            pbBarraProgreso.Value = 100;
            btnEjecutar.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtEdad.Text))
            {
                errorProvider.SetError(txtEdad, "Se debe ingresar la edad");
                return;
            }

            int edad = 0;

            if (!int.TryParse(txtEdad.Text, out edad))
            {
                errorProvider.SetError(txtEdad, "La edad debe ser un número entero");
                return;
            }

            errorProvider.SetError(txtEdad, "");
            MessageBox.Show("La edad es correcta");
        }
    }
}
