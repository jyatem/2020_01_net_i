﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Controles
{
    public partial class FormControles : Form
    {
        public FormControles()
        {
            InitializeComponent();
        }

        private void btnObtenerInformacion_Click(object sender, EventArgs e)
        {
            lblNombre.Text = "Nombre cambiado";

            string nombre = txtNombre.Text;
            string pais = cmbPaises.Text;
            string hobbies = "";
            string lenguajes = "";
            string genero = rdoFemenino.Checked ? "Femenino" : "Masculino";
            string estadoCivil = rdoSoltero.Checked ? "Soltero" : "Casado";
            DateTime fecha = dtpFecha.Value;
            string activo = chkActivo.Checked ? "Activo" : "Inactivo";
            
            foreach (var item in lstHobbies.SelectedItems)
            {
                hobbies += item.ToString() + ", ";
            }

            foreach (var item in chkLstLenguajes.CheckedItems)
            {
                lenguajes += item.ToString() + ", ";
            }

            MessageBox.Show(String.Format("Nombre: {0}{1}País: {2}{1}Hobbies: {3}{1}Lenguajes: {4}{1}Género: {5}{1}Estado civil: {6}{1}Fecha: {7:yyyy.MM.dd}{1}Estado: {8}", nombre, Environment.NewLine, pais, hobbies, lenguajes, genero, estadoCivil, fecha, activo));
        }

        private void mtxtFecha_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            //if (!e.IsValidInput)
            //{
            //    MessageBox.Show("La fecha no es valida");
            //    mtxtFecha.Focus();
            //}
        }
    }
}
