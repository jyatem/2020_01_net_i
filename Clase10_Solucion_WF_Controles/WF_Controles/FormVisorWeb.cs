﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Controles
{
    public partial class FormVisorWeb : Form
    {
        public FormVisorWeb()
        {
            InitializeComponent();
        }

        private void btnIr_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"^http\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$");

            Match match = regex.Match(txtURL.Text);

            if (match.Success)
            {
                webBrowser.Navigate(txtURL.Text);
            }
            else
            {
                MessageBox.Show("La URL no es correcta");
            }            
        }
    }
}
