﻿namespace WF_Controles
{
    partial class FormControles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormControles));
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnObtenerInformacion = new System.Windows.Forms.Button();
            this.mtxtFecha = new System.Windows.Forms.MaskedTextBox();
            this.lblAyudaFecha = new System.Windows.Forms.Label();
            this.rtxtInformacion = new System.Windows.Forms.RichTextBox();
            this.cmbPaises = new System.Windows.Forms.ComboBox();
            this.lstHobbies = new System.Windows.Forms.ListBox();
            this.chkLstLenguajes = new System.Windows.Forms.CheckedListBox();
            this.rdoFemenino = new System.Windows.Forms.RadioButton();
            this.rdoMasculino = new System.Windows.Forms.RadioButton();
            this.gbEstadoCivil = new System.Windows.Forms.GroupBox();
            this.rdoSoltero = new System.Windows.Forms.RadioButton();
            this.rdoCasado = new System.Windows.Forms.RadioButton();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.gbEstadoCivil.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(29, 13);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(29, 36);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(393, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // btnObtenerInformacion
            // 
            this.btnObtenerInformacion.Location = new System.Drawing.Point(138, 513);
            this.btnObtenerInformacion.Name = "btnObtenerInformacion";
            this.btnObtenerInformacion.Size = new System.Drawing.Size(177, 23);
            this.btnObtenerInformacion.TabIndex = 4;
            this.btnObtenerInformacion.Text = "Obtener Información";
            this.btnObtenerInformacion.UseVisualStyleBackColor = true;
            this.btnObtenerInformacion.Click += new System.EventHandler(this.btnObtenerInformacion_Click);
            // 
            // mtxtFecha
            // 
            this.mtxtFecha.Location = new System.Drawing.Point(29, 66);
            this.mtxtFecha.Mask = "00/00/0000";
            this.mtxtFecha.Name = "mtxtFecha";
            this.mtxtFecha.Size = new System.Drawing.Size(100, 20);
            this.mtxtFecha.TabIndex = 2;
            this.mtxtFecha.ValidatingType = typeof(System.DateTime);
            this.mtxtFecha.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.mtxtFecha_TypeValidationCompleted);
            // 
            // lblAyudaFecha
            // 
            this.lblAyudaFecha.AutoSize = true;
            this.lblAyudaFecha.Location = new System.Drawing.Point(135, 69);
            this.lblAyudaFecha.Name = "lblAyudaFecha";
            this.lblAyudaFecha.Size = new System.Drawing.Size(71, 13);
            this.lblAyudaFecha.TabIndex = 3;
            this.lblAyudaFecha.Text = "(dd/mm/yyyy)";
            // 
            // rtxtInformacion
            // 
            this.rtxtInformacion.Location = new System.Drawing.Point(29, 96);
            this.rtxtInformacion.Name = "rtxtInformacion";
            this.rtxtInformacion.Size = new System.Drawing.Size(393, 66);
            this.rtxtInformacion.TabIndex = 5;
            this.rtxtInformacion.Text = "";
            // 
            // cmbPaises
            // 
            this.cmbPaises.FormattingEnabled = true;
            this.cmbPaises.Items.AddRange(new object[] {
            "Colombia",
            "Argentina",
            "Ecuador",
            "Perú"});
            this.cmbPaises.Location = new System.Drawing.Point(29, 169);
            this.cmbPaises.Name = "cmbPaises";
            this.cmbPaises.Size = new System.Drawing.Size(162, 21);
            this.cmbPaises.TabIndex = 6;
            // 
            // lstHobbies
            // 
            this.lstHobbies.FormattingEnabled = true;
            this.lstHobbies.Items.AddRange(new object[] {
            "Música",
            "Cine",
            "Xbox",
            "Bicicleta"});
            this.lstHobbies.Location = new System.Drawing.Point(29, 197);
            this.lstHobbies.Name = "lstHobbies";
            this.lstHobbies.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstHobbies.Size = new System.Drawing.Size(236, 43);
            this.lstHobbies.TabIndex = 7;
            // 
            // chkLstLenguajes
            // 
            this.chkLstLenguajes.FormattingEnabled = true;
            this.chkLstLenguajes.Items.AddRange(new object[] {
            "C#",
            "Visual Basic",
            "PHP",
            "HTML",
            "JavaScript"});
            this.chkLstLenguajes.Location = new System.Drawing.Point(29, 247);
            this.chkLstLenguajes.Name = "chkLstLenguajes";
            this.chkLstLenguajes.Size = new System.Drawing.Size(236, 49);
            this.chkLstLenguajes.TabIndex = 8;
            // 
            // rdoFemenino
            // 
            this.rdoFemenino.AutoSize = true;
            this.rdoFemenino.Checked = true;
            this.rdoFemenino.Location = new System.Drawing.Point(32, 312);
            this.rdoFemenino.Name = "rdoFemenino";
            this.rdoFemenino.Size = new System.Drawing.Size(71, 17);
            this.rdoFemenino.TabIndex = 9;
            this.rdoFemenino.TabStop = true;
            this.rdoFemenino.Text = "Femenino";
            this.rdoFemenino.UseVisualStyleBackColor = true;
            // 
            // rdoMasculino
            // 
            this.rdoMasculino.AutoSize = true;
            this.rdoMasculino.Location = new System.Drawing.Point(121, 312);
            this.rdoMasculino.Name = "rdoMasculino";
            this.rdoMasculino.Size = new System.Drawing.Size(73, 17);
            this.rdoMasculino.TabIndex = 10;
            this.rdoMasculino.Text = "Masculino";
            this.rdoMasculino.UseVisualStyleBackColor = true;
            // 
            // gbEstadoCivil
            // 
            this.gbEstadoCivil.Controls.Add(this.rdoCasado);
            this.gbEstadoCivil.Controls.Add(this.rdoSoltero);
            this.gbEstadoCivil.Location = new System.Drawing.Point(32, 346);
            this.gbEstadoCivil.Name = "gbEstadoCivil";
            this.gbEstadoCivil.Size = new System.Drawing.Size(218, 63);
            this.gbEstadoCivil.TabIndex = 11;
            this.gbEstadoCivil.TabStop = false;
            this.gbEstadoCivil.Text = "Estado civil";
            // 
            // rdoSoltero
            // 
            this.rdoSoltero.AutoSize = true;
            this.rdoSoltero.Checked = true;
            this.rdoSoltero.Location = new System.Drawing.Point(21, 28);
            this.rdoSoltero.Name = "rdoSoltero";
            this.rdoSoltero.Size = new System.Drawing.Size(58, 17);
            this.rdoSoltero.TabIndex = 0;
            this.rdoSoltero.TabStop = true;
            this.rdoSoltero.Text = "Soltero";
            this.rdoSoltero.UseVisualStyleBackColor = true;
            // 
            // rdoCasado
            // 
            this.rdoCasado.AutoSize = true;
            this.rdoCasado.Location = new System.Drawing.Point(106, 28);
            this.rdoCasado.Name = "rdoCasado";
            this.rdoCasado.Size = new System.Drawing.Size(61, 17);
            this.rdoCasado.TabIndex = 1;
            this.rdoCasado.Text = "Casado";
            this.rdoCasado.UseVisualStyleBackColor = true;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(32, 416);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(298, 20);
            this.dtpFecha.TabIndex = 12;
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Location = new System.Drawing.Point(32, 451);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(56, 17);
            this.chkActivo.TabIndex = 13;
            this.chkActivo.Text = "Activo";
            this.chkActivo.UseVisualStyleBackColor = true;
            // 
            // FormControles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(460, 548);
            this.Controls.Add(this.chkActivo);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.gbEstadoCivil);
            this.Controls.Add(this.rdoMasculino);
            this.Controls.Add(this.rdoFemenino);
            this.Controls.Add(this.chkLstLenguajes);
            this.Controls.Add(this.lstHobbies);
            this.Controls.Add(this.cmbPaises);
            this.Controls.Add(this.rtxtInformacion);
            this.Controls.Add(this.lblAyudaFecha);
            this.Controls.Add(this.mtxtFecha);
            this.Controls.Add(this.btnObtenerInformacion);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNombre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FormControles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controles";
            this.gbEstadoCivil.ResumeLayout(false);
            this.gbEstadoCivil.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnObtenerInformacion;
        private System.Windows.Forms.MaskedTextBox mtxtFecha;
        private System.Windows.Forms.Label lblAyudaFecha;
        private System.Windows.Forms.RichTextBox rtxtInformacion;
        private System.Windows.Forms.ComboBox cmbPaises;
        private System.Windows.Forms.ListBox lstHobbies;
        private System.Windows.Forms.CheckedListBox chkLstLenguajes;
        private System.Windows.Forms.RadioButton rdoFemenino;
        private System.Windows.Forms.RadioButton rdoMasculino;
        private System.Windows.Forms.GroupBox gbEstadoCivil;
        private System.Windows.Forms.RadioButton rdoCasado;
        private System.Windows.Forms.RadioButton rdoSoltero;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.CheckBox chkActivo;
    }
}

