﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Ciudad
    {
        public int Id { get; set; }

        public string NombreCiudad { get; set; }

        public Departamento Departamento { get; set; }

        public string NombreDepartamento
        {
            get { return Departamento.NombreDepartamento; }
        }

    }
}
