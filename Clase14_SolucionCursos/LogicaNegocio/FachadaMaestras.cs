﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaMaestras
    {
        private DAODepartamento _daoDepartamento;
        private DAOCiudad _daoCiudad;
        private DAOCurso _daoCurso;

        public FachadaMaestras()
        {
            _daoDepartamento = new DAODepartamento();
            _daoCiudad = new DAOCiudad();
            _daoCurso = new DAOCurso();
        }

        #region [ Operaciones para Departamento ]
        public List<Departamento> RetornarDepartamentos()
        {
            return _daoDepartamento.RetornarDepartamentos();
        }
        #endregion

        #region [ Operaciones para Ciudad ]
        public List<Ciudad> RetornarCiudades()
        {
            return _daoCiudad.RetornarCiudades();
        }

        public void ActualizarCiudad(Ciudad ciudad)
        {
            try
            {
                int cantidadRegistrosActualizados = _daoCiudad.ActualizarCiudad(ciudad);

                if (cantidadRegistrosActualizados == 0)
                {
                    throw new Exception($"No existe la ciudad con Id: {ciudad.Id}");
                }
                else if (cantidadRegistrosActualizados > 1)
                {
                    throw new Exception($"Se actualizaron {cantidadRegistrosActualizados} ciudades");
                }
            }
            catch (Exception)
            {
                // Guardar archivo Log. Enviar Correo. ex.Message o ex.StactTrace
                throw;
            }
        }

        public void EliminarCiudad(int idCiudad)
        {
            int registrosEliminados = _daoCiudad.EliminarCiudad(idCiudad);

            if (registrosEliminados == 0)
            {
                throw new Exception($"No existe la ciudad con Id: {idCiudad}");
            }
            else if (registrosEliminados > 1)
            {
                throw new Exception($"Se eliminaron {registrosEliminados} ciudades");
            }
        }

        public Ciudad RetornarCiudad(int idCiudad)
        {
            Ciudad ciudad = _daoCiudad.RetornarCiudad(idCiudad);

            if (ciudad == null)
            {
                throw new Exception($"La ciudad {idCiudad} no existe");
            }

            return ciudad;
        }
        #endregion

        #region [ Operaciones para Curso ]
        public DataSet RetornarCursos()
        {
            return _daoCurso.RetornarCursos();
        }
        #endregion
    }
}
