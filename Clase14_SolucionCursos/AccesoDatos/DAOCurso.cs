﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAOCurso
    {
        public DataSet RetornarCursos()
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT * FROM Curso; SELECT * FROM Ciudad", conexion);

                DataSet dataSet = new DataSet();

                conexion.Open();
                sqlDataAdapter.Fill(dataSet);
                conexion.Close();

                return dataSet;
            }
        }
    }
}
