﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCursos : Form
    {
        private FachadaMaestras _fachadaMaestras;

        public FormCursos()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        private void FormCursos_Load(object sender, EventArgs e)
        {
            DataSet dataSet = _fachadaMaestras.RetornarCursos();

            dgvCursos.DataSource = dataSet.Tables[1];

            foreach (Ciudad ciudad in _fachadaMaestras.RetornarCiudades())
            {
                int id = ciudad.Id;
                string nombre = ciudad.NombreCiudad;
            }

            foreach (DataRow dataRow in dataSet.Tables[1].Rows)
            {
                int id = Convert.ToInt32(dataRow["Id"]);
            }
        }
    }
}
