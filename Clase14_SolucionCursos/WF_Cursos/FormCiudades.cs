﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCiudades : Form
    {
        private FachadaMaestras _fachadaMaestras;

        public FormCiudades()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        private void FormCiudades_Load(object sender, EventArgs e)
        {
            dgvCiudades.AutoGenerateColumns = false;
            dgvCiudades.DataSource = _fachadaMaestras.RetornarCiudades();
        }

        private void dgvCiudades_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView dataGridView = sender as DataGridView;

                if (dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == 3)
                    {
                        // ACTUALIZAR
                        int idCiudad = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value);

                        FormCiudad formCiudad = new FormCiudad(idCiudad);
                        formCiudad.ShowDialog();
                    }
                    else if (e.ColumnIndex == 4)
                    {
                        // ELIMINAR
                        DialogResult dialogResult = MessageBox.Show("¿Realmente quiere eliminar la ciudad?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                        if (dialogResult == DialogResult.Yes)
                        {
                            int idCiudad = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value);

                            _fachadaMaestras.EliminarCiudad(idCiudad);
                            MessageBox.Show("Ciudad eliminada exitosamente");
                            //MessageBox.Show($"IdCiudad: {idCiudad}");
                        }
                    }

                    dataGridView.DataSource = _fachadaMaestras.RetornarCiudades();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
