﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCiudad : Form
    {
        private FachadaMaestras _fachadaMaestras;
        private int _idCiudad;

        public FormCiudad()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        public FormCiudad(int idCiudad)
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();

            txtId.Text = idCiudad.ToString();
            txtId.Enabled = false;

            _idCiudad = idCiudad;
        }

        private void FormCiudad_Load(object sender, EventArgs e)
        {
            try
            {
                List<Departamento> departamentos = _fachadaMaestras.RetornarDepartamentos();

                departamentos.Insert(0, new Departamento { Id = 0, NombreDepartamento = "-- Seleccione un departamento --" });

                cmbDepartamento.DataSource = departamentos;
                cmbDepartamento.DisplayMember = "NombreDepartamento";
                cmbDepartamento.ValueMember = "Id";

                if (_idCiudad > 0)
                {
                    Ciudad ciudad = _fachadaMaestras.RetornarCiudad(_idCiudad);

                    if (ciudad != null)
                    {
                        txtNombre.Text = ciudad.NombreCiudad;
                        cmbDepartamento.SelectedValue = ciudad.Departamento.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Ciudad ciudad = new Ciudad
                {
                    Id = Convert.ToInt32(txtId.Text),
                    NombreCiudad = txtNombre.Text,
                    Departamento = new Departamento
                    {
                        Id = Convert.ToInt32(cmbDepartamento.SelectedValue)
                    }
                };

                _fachadaMaestras.ActualizarCiudad(ciudad);

                MessageBox.Show("Ciudad actualizada exitosamente");

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
