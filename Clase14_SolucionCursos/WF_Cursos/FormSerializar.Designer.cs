﻿namespace WF_Cursos
{
    partial class FormSerializar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnGenerarXML = new System.Windows.Forms.Button();
            this.btnGenerarObjeto = new System.Windows.Forms.Button();
            this.btnGenerarJSON = new System.Windows.Forms.Button();
            this.rtxtResultado = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(156, 33);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 0;
            // 
            // btnGenerarXML
            // 
            this.btnGenerarXML.Location = new System.Drawing.Point(23, 78);
            this.btnGenerarXML.Name = "btnGenerarXML";
            this.btnGenerarXML.Size = new System.Drawing.Size(119, 23);
            this.btnGenerarXML.TabIndex = 1;
            this.btnGenerarXML.Text = "Generar XML";
            this.btnGenerarXML.UseVisualStyleBackColor = true;
            this.btnGenerarXML.Click += new System.EventHandler(this.btnGenerarXML_Click);
            // 
            // btnGenerarObjeto
            // 
            this.btnGenerarObjeto.Location = new System.Drawing.Point(146, 78);
            this.btnGenerarObjeto.Name = "btnGenerarObjeto";
            this.btnGenerarObjeto.Size = new System.Drawing.Size(119, 23);
            this.btnGenerarObjeto.TabIndex = 2;
            this.btnGenerarObjeto.Text = "Generar Objeto";
            this.btnGenerarObjeto.UseVisualStyleBackColor = true;
            this.btnGenerarObjeto.Click += new System.EventHandler(this.btnGenerarObjeto_Click);
            // 
            // btnGenerarJSON
            // 
            this.btnGenerarJSON.Location = new System.Drawing.Point(269, 78);
            this.btnGenerarJSON.Name = "btnGenerarJSON";
            this.btnGenerarJSON.Size = new System.Drawing.Size(119, 23);
            this.btnGenerarJSON.TabIndex = 3;
            this.btnGenerarJSON.Text = "GenerarJSON";
            this.btnGenerarJSON.UseVisualStyleBackColor = true;
            this.btnGenerarJSON.Click += new System.EventHandler(this.btnGenerarJSON_Click);
            // 
            // rtxtResultado
            // 
            this.rtxtResultado.Location = new System.Drawing.Point(23, 119);
            this.rtxtResultado.Name = "rtxtResultado";
            this.rtxtResultado.Size = new System.Drawing.Size(365, 235);
            this.rtxtResultado.TabIndex = 4;
            this.rtxtResultado.Text = "";
            // 
            // FormSerializar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 381);
            this.Controls.Add(this.rtxtResultado);
            this.Controls.Add(this.btnGenerarJSON);
            this.Controls.Add(this.btnGenerarObjeto);
            this.Controls.Add(this.btnGenerarXML);
            this.Controls.Add(this.txtId);
            this.Name = "FormSerializar";
            this.Text = "FormSerializar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Button btnGenerarXML;
        private System.Windows.Forms.Button btnGenerarObjeto;
        private System.Windows.Forms.Button btnGenerarJSON;
        private System.Windows.Forms.RichTextBox rtxtResultado;
    }
}