﻿using Entidades;
using LogicaNegocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace WF_Cursos
{
    public partial class FormSerializar : Form
    {
        private FachadaMaestras _fachadaMaestras;

        public FormSerializar()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        private void btnGenerarXML_Click(object sender, EventArgs e)
        {
            try
            {
                Ciudad ciudad = _fachadaMaestras.RetornarCiudad(Convert.ToInt32(txtId.Text));

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Ciudad));

                StringWriter stringWriter = new StringWriter();

                xmlSerializer.Serialize(stringWriter, ciudad);

                rtxtResultado.Text = stringWriter.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGenerarObjeto_Click(object sender, EventArgs e)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Ciudad));

                StringReader stringReader = new StringReader(rtxtResultado.Text);

                XmlTextReader xmlTextReader = new XmlTextReader(stringReader);

                Ciudad ciudad = xmlSerializer.Deserialize(xmlTextReader) as Ciudad;

                MessageBox.Show(ciudad.NombreCiudad + " de " + ciudad.Departamento.NombreDepartamento);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGenerarJSON_Click(object sender, EventArgs e)
        {
            try
            {
                Ciudad ciudad = _fachadaMaestras.RetornarCiudad(Convert.ToInt32(txtId.Text));

                rtxtResultado.Text = JsonConvert.SerializeObject(ciudad);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
