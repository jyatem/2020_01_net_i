﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormTabla.aspx.cs" Inherits="AplicacionWeb.WebFormTabla" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <table border="1">

            <tr>
                <td>Dato 1</td>
                <td>Dato 2</td>
                <td>Dato 3</td>
            </tr>

            <tr>
                <td>Dato 4</td>
                <td>Dato 5</td>
                <td>Dato 6</td>
            </tr>

            <tr>
                <td>Dato 7</td>
                <td colspan="2">
                    Dato 8
                </td>
            </tr>

        </table>

    </form>
</body>
</html>
