﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormControles.aspx.cs" Inherits="AplicacionWeb.WebFormControles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table border="1">
            <tr>
                <th>Nombre del control</th>
                <th>Control</th>
            </tr>

            <tr>
                <td>Calendario</td>
                <td>
                    <asp:Calendar ID="calFecha" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px">
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <WeekendDayStyle BackColor="#98a3c6" />
                    </asp:Calendar>
                </td>
            </tr>

            <tr>
                <td>Checkbox</td>
                <td>
                    <asp:CheckBox ID="chkTieneCarro" runat="server" Text="Tiene carro" Checked="true"/>
                </td>
            </tr>

            <tr>
                <td>CheckboxList</td>
                <td>
                    <asp:CheckBoxList ID="chkLenguajes" runat="server">
                        <asp:ListItem>C#</asp:ListItem>
                        <asp:ListItem>Html</asp:ListItem>
                        <asp:ListItem>Javascript</asp:ListItem>
                        <asp:ListItem>CSS</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>

            <tr>
                <td>DropDownList (ComboBox)</td>
                <td>
                    <asp:DropDownList ID="ddlCiudad" runat="server">
                        <asp:ListItem>Medellin</asp:ListItem>
                        <asp:ListItem>Itagui</asp:ListItem>
                        <asp:ListItem>Envigado</asp:ListItem>
                        <asp:ListItem>Bello</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td>Hidden Field</td>
                <td>
                    <asp:HiddenField ID="hdfValorOculto" runat="server" Value="100"/>
                </td>
            </tr>

            <tr>
                <td>Hyperlink</td>
                <td>
                    <asp:HyperLink ID="hlURL" runat="server" NavigateUrl="http://www.google.com" Target="_blank">Google</asp:HyperLink>
                </td>
            </tr>

            <tr>
                <td>Paginas del sitio</td>
                <td>
                    <asp:HyperLink ID="hlHolaWeb" runat="server" NavigateUrl="~/WebFormHolaWebApp.aspx" Target="_blank">Hola Web App</asp:HyperLink>
                </td>
            </tr>

            <tr>
                <td>Imagen</td>
                <td>
                    <asp:Image ID="imgLogo" runat="server" ImageUrl="~/Imagenes/Logo.jpg" Width="100px" Height="100px"/>
                </td>
            </tr>

            <tr>
                <td>ListBox</td>
                <td>
                    <asp:ListBox ID="lstPasatiempos" runat="server" Width="100px" SelectionMode="Multiple">
                        <asp:ListItem>xBox</asp:ListItem>
                        <asp:ListItem>Cine</asp:ListItem>
                        <asp:ListItem>Libros</asp:ListItem>
                        <asp:ListItem>Fútbol</asp:ListItem>
                    </asp:ListBox>
                </td>
            </tr>

            <tr>
                <td>RadioButton</td>
                <td>
                    <asp:RadioButton ID="rdoVerdadero" runat="server" Text="Verdadero" GroupName="radio" Checked="true"/>
                    <asp:RadioButton ID="rdoFalso" runat="server" Text="Falso" GroupName="radio"/>
                </td>
            </tr>

            <tr>
                <td>RadioButtonList</td>
                <td>
                    <asp:RadioButtonList ID="rdoLstOpciones" runat="server">
                        <asp:ListItem>Opcion 1</asp:ListItem>
                        <asp:ListItem Selected="True">Opcion 2</asp:ListItem>
                        <asp:ListItem>Opcion 3</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>

            <tr>
                <td>Textbox Multilinea</td>
                <td>
                    <asp:TextBox ID="txtMultilinea" runat="server" TextMode="MultiLine" Height="50px"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:Button ID="btnObtenerInformacion" runat="server" Text="Obtener Información" OnClick="btnObtenerInformacion_Click"/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:Label ID="lblValores" runat="server" ></asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
