﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AplicacionWeb
{
    public partial class WebFormControles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnObtenerInformacion_Click(object sender, EventArgs e)
        {
            /* Información:
             * calFecha
             * chkTieneCarro
             * chkLenguajes
             * ddlCiudad
             * hdfValorOculto
             * lstPasatiempos
             * rdoVerdadero y rdoFalso
             * rdoLstOpciones
             * Text Multilinea
             * <br/>
             */

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(calFecha.SelectedDate.ToString("yyyy-MM-dd") + "<br/>");
            stringBuilder.Append(chkTieneCarro.Checked ? "Tiene carro<br/>" : "No tiene carro<br/>");

            foreach (ListItem item in chkLenguajes.Items)
            {
                if (item.Selected)
                {
                    stringBuilder.Append(item.Text + "<br/>");
                }
            }

            stringBuilder.Append(ddlCiudad.SelectedValue + "<br/>");
            stringBuilder.Append(hdfValorOculto.Value + "<br/>");

            foreach (ListItem item in lstPasatiempos.Items)
            {
                if (item.Selected)
                {
                    stringBuilder.Append(item.Text + "<br/>");
                }
            }

            stringBuilder.Append(rdoVerdadero.Checked ? "Verdadero<br/>" : "Falso<br/>");
            stringBuilder.Append(rdoLstOpciones.SelectedValue + "<br/>");

            lblValores.Text = stringBuilder.ToString();
        }
    }
}