﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class LogicaCurso
    {
        public Curso MiCurso { get; set; }

        //public List<Estudiante> Estudiantes 
        //{
        //    get
        //    {
        //        return MiCurso.Estudiantes;
        //    }
        //}

        public LogicaCurso(string codigoCurso, string nombreCurso, int duracion)
        {
            MiCurso = new Curso { CodigoCurso = codigoCurso, NombreCurso = nombreCurso, Duracion = duracion };
        }

        public void AgregarEstudiante(Estudiante estudiante)
        {
            MiCurso.Estudiantes.Add(estudiante);
        }
    }
}
