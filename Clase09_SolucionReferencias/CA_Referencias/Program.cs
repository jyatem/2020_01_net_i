﻿using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Referencias
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instanciar LogicaCurso
            LogicaCurso logicaCurso = new LogicaCurso("01", ".NET Básico", 110);

            // Agregar 3 estudiantes 
            Estudiante estudiante1 = new Estudiante { Id = 1, Nombre = "Jairo", Apellidos = "Yate" };

            logicaCurso.AgregarEstudiante(estudiante1);
            logicaCurso.AgregarEstudiante(new Estudiante { Id = 2, Nombre = "Lina", Apellidos = "Mora" });
            logicaCurso.AgregarEstudiante(new Estudiante { Id = 3, Nombre = "Mariana", Apellidos = "Yate" });

            // foreach Muestren el nombre y apellido de los estudiantes
            foreach (Estudiante estudiante in logicaCurso.MiCurso.Estudiantes)
            {
                Console.WriteLine($"Estudiante: {estudiante.Nombre} {estudiante.Apellidos}");
            }

            Console.ReadLine();
        }
    }
}
