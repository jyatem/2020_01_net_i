﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Curso
    {
        public string CodigoCurso { get; set; }

        public string NombreCurso { get; set; }

        public int Duracion { get; set; }

        public List<Estudiante> Estudiantes { get; set; }

        public Curso()
        {
            Estudiantes = new List<Estudiante>();
        }
    }
}
