﻿using CursoClienteServicioWeb.ServicioWebCursos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursoClienteServicioWeb
{
    public partial class WebFormSumar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSumar_Click(object sender, EventArgs e)
        {
            ServicioCursoClient cliente = new ServicioCursoClient();

            lblInformacion.Text = cliente.Sumar(Convert.ToInt32(txtValor1.Text), Convert.ToInt32(txtValor2.Text)).ToString();
            
            cliente.Close();
        }
    }
}