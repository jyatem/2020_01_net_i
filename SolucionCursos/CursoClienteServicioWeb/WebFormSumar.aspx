﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormSumar.aspx.cs" Inherits="CursoClienteServicioWeb.WebFormSumar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtValor1" runat="server"></asp:TextBox><br />
            <asp:TextBox ID="txtValor2" runat="server"></asp:TextBox><br />
            <asp:Button ID="btnSumar" runat="server" Text="Sumar" OnClick="btnSumar_Click"/><br />
            <asp:Label ID="lblInformacion" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
