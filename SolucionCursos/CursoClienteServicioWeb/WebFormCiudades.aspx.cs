﻿using CursoClienteServicioWeb.ServicioWebCursos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursoClienteServicioWeb
{
    public partial class WebFormCiudades : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicioCursoClient cliente = new ServicioCursoClient();

            gvCiudades.DataSource = cliente.RetornarCiudades();
            gvCiudades.DataBind();

            cliente.Close();
        }
    }
}