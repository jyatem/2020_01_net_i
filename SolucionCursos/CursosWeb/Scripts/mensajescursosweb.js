﻿function mensajeEliminar(name, mensaje) {
    $('<div id="dialog-message" title="Confirmación Eliminar"><p>' + mensaje + '</p></div>').dialog(
        {
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Sí": function () {
                    $(this).dialog("close");
                    __doPostBack(name, '');
                },
                "No": function () {
                    $(this).dialog("close");
                }
            }
        }).prev(".ui-dialog-titlebar").css("background", "#323a40").css("color", "white");

    return false;
}