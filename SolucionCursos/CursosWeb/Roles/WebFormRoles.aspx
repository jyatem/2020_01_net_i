﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormRoles.aspx.cs" Inherits="CursosWeb.Roles.WebFormRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Roles
            </div>
            <div class="card-body">
                <%--Nombre--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtRol">Rol:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtRol" runat="server" CssClass="form-control" Placeholder="Ingrese el nombre del rol" MaxLength="250" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnCrearRol" runat="server" Text="Crear rol" CssClass="btn btn-dark" OnClick="btnCrearRol_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacionRol" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-info">
                Asignar Roles
            </div>
            <div class="card-body">
                <%--Usuarios--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Usuarios:</label>
                    <div class="col-md-10">
                        <asp:DropDownList ID="ddlUsuarios" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>

                <%--Roles--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Roles:</label>
                    <div class="col-md-10">
                        <asp:DropDownList ID="ddlRoles" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnAsignarRol" runat="server" Text="Asignar rol" CssClass="btn btn-info" OnClick="btnAsignarRol_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacionAsignarRol" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

                <asp:GridView ID="gvRolesUsuarios" runat="server" CssClass="table table-striped table-bordered"></asp:GridView>

            </div>
        </div>
    </div>

</asp:Content>
