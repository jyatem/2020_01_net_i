﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Roles
{
    public partial class WebFormRoles : System.Web.UI.Page
    {
        private GestionRoles _gestionRoles = new GestionRoles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LlenarUsuarios();
                LlenarRoles();
                LlenarGrid();
            }
        }

        protected void btnCrearRol_Click(object sender, EventArgs e)
        {
            try
            {
                _gestionRoles.CrearRol(txtRol.Text);
                lblInformacionRol.CssClass = "text-success";
                lblInformacionRol.Text = "Rol creado exitosamente";
                LlenarRoles();
            }
            catch (Exception ex)
            {
                lblInformacionRol.CssClass = "text-danger";
                lblInformacionRol.Text = ex.Message;
            }
        }

        protected void btnAsignarRol_Click(object sender, EventArgs e)
        {
            try
            {
                _gestionRoles.AsignarRol(ddlUsuarios.SelectedValue, ddlRoles.SelectedValue);
                lblInformacionAsignarRol.CssClass = "text-success";
                lblInformacionAsignarRol.Text = "Rol asignado exitosamente";
                LlenarGrid();
            }
            catch (Exception ex)
            {
                lblInformacionRol.CssClass = "text-danger";
                lblInformacionRol.Text = ex.Message;
            }
        }

        private void LlenarUsuarios()
        {
            ddlUsuarios.DataSource = _gestionRoles.RetornarUsuarios();
            ddlUsuarios.DataTextField = "UserName";
            ddlUsuarios.DataValueField = "UserName";
            ddlUsuarios.DataBind();

            ddlUsuarios.Items.Insert(0, new ListItem("-- Seleccione un usuario --", ""));
        }

        private void LlenarRoles()
        {
            ddlRoles.DataSource = _gestionRoles.RetornarRoles();
            ddlRoles.DataTextField = "Name";
            ddlRoles.DataValueField = "Name";
            ddlRoles.DataBind();

            ddlRoles.Items.Insert(0, new ListItem("-- Seleccione un rol --", ""));
        }

        private void LlenarGrid()
        {
            gvRolesUsuarios.DataSource = _gestionRoles.RetornarUsuariosRol();
            gvRolesUsuarios.DataBind();
        }
    }
}