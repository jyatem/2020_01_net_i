﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace CursosWeb.Roles
{
    public class GestionRoles
    {
        private UserStore<IdentityUser> _almacenamientoUsuarios;
        private UserManager<IdentityUser> _administracionUsuarios;

        private RoleStore<IdentityRole> _almacenamientoRoles;
        private RoleManager<IdentityRole> _administracionRoles;

        public GestionRoles()
        {
            _almacenamientoUsuarios = new UserStore<IdentityUser>();
            _administracionUsuarios = new UserManager<IdentityUser>(_almacenamientoUsuarios);

            _almacenamientoRoles = new RoleStore<IdentityRole>();
            _administracionRoles = new RoleManager<IdentityRole>(_almacenamientoRoles);
        }

        public bool CrearRol(string nombreRol)
        {
            IdentityResult identityResult = _administracionRoles.Create(new IdentityRole(nombreRol));
            return identityResult.Succeeded;
        }

        public List<IdentityRole> RetornarRoles()
        {
            return _administracionRoles.Roles.ToList();
        }

        public List<IdentityUser> RetornarUsuarios()
        {
            return _administracionUsuarios.Users.ToList();
        }

        public bool AsignarRol(string nombreUsuario, string nombreRol)
        {
            var usuario = _administracionUsuarios.FindByName(nombreUsuario);

            if (!_administracionUsuarios.IsInRole(usuario.Id, nombreRol))
            {
                IdentityResult identityResult = _administracionUsuarios.AddToRole(usuario.Id, nombreRol);

                return identityResult.Succeeded;
            }

            return false;
        }

        public List<DTOUsuarioRol> RetornarUsuariosRol()
        {
            List<DTOUsuarioRol> usuariosRoles = _administracionUsuarios.Users.Select(u => new DTOUsuarioRol { Usuario = u.UserName, Rol = "" }).ToList();

            foreach (var usuario in usuariosRoles)
            {
                IdentityUser identityUser = _administracionUsuarios.FindByName(usuario.Usuario);

                foreach (var itemRol in identityUser.Roles)
                {
                    IdentityRole rol = _administracionRoles.FindById(itemRol.RoleId);
                    usuario.Rol += rol.Name + ", ";
                }

                if (usuario.Rol.IndexOf(",") > 0)
                {
                    usuario.Rol = usuario.Rol.Remove(usuario.Rol.LastIndexOf(","), 1);
                }
            }

            return usuariosRoles;
        }
    }
}