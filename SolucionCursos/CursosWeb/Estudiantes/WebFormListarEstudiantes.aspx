﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormListarEstudiantes.aspx.cs" Inherits="CursosWeb.Estudiantes.WebFormListarEstudiantes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Content/paginacion.css" rel="stylesheet" />

    <div class="form-inline mt-3">
        <div class="form-group">
            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control mr-2" placeholder="Nombre del estudiante"></asp:TextBox>
            <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control mr-2" placeholder="Apellido del estudiante"></asp:TextBox>
            <asp:DropDownList ID="ddlCiudadNacimiento" runat="server" CssClass="form-control mr-2"></asp:DropDownList>
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click"/>
        </div>
    </div>
    
    <asp:GridView ID="gvEstudiantes" runat="server" CssClass="table table-striped table-dark table-hover mt-3" AutoGenerateColumns="false" AllowPaging="true" PageSize="2" OnPageIndexChanging="gvEstudiantes_PageIndexChanging" OnRowCommand="gvEstudiantes_RowCommand" EmptyDataText="No existen estudiantes" AllowSorting="true" OnSorting="gvEstudiantes_Sorting">
        <Columns>
            <asp:HyperLinkField DataTextField="Cedula" HeaderText="Cédula" DataNavigateUrlFormatString="~/Estudiantes/WebFormEstudiante.aspx?Cedula={0}" DataNavigateUrlFields="Cedula" />
            <%--<asp:BoundField HeaderText="Cédula" DataField="Cedula" />--%>
            <asp:BoundField HeaderText="Nombre Completo" DataField="NombreCompleto" SortExpression="Nombre"/>
            <asp:BoundField HeaderText="Ciudad de Nac." DataField="NombreCiudadNacimiento" />
            <asp:BoundField HeaderText="Genero" DataField="Genero" />
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Realmente desea eliminar este estudiante?');" CommandName="Eliminar" CommandArgument='<%# Bind("Cedula") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>

</asp:Content>
