﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Estudiantes
{
    public partial class WebFormListarEstudiantes : System.Web.UI.Page
    {
        #region [ Variables privadas ]
        private FachadaEstudiante _fachadaEstudiante = new FachadaEstudiante();
        private FachadaCiudad _fachadaCiudad = new FachadaCiudad();
        private string _ordenarPor = "";
        private string _tipoOrdenamiento = "";
        #endregion

        #region [ Eventos de la página ]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarCiudades();
                CargarEstudiantes();
            }
        }

        protected void gvEstudiantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (ViewState["TipoOrdenamiento"] == null)
            {
                _tipoOrdenamiento = ViewState["TipoOrdenamiento"].ToString();
            }

            if (ViewState["OrdenarPor"] == null)
            {
                _ordenarPor = ViewState["OrdenarPor"].ToString();
            }

            gvEstudiantes.PageIndex = e.NewPageIndex;
            CargarEstudiantes();
        }

        protected void gvEstudiantes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Eliminar")
            {
                int cedula = Convert.ToInt32(e.CommandArgument);

                if (_fachadaEstudiante.EliminarEstudiante(cedula) == 1)
                {
                    CargarEstudiantes();
                    lblInformacion.CssClass = "text-success";
                    lblInformacion.Text = $"El estudiante se eliminó correctamente ({DateTime.Now.ToString()})";
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = $"El estudiante no se pudo eliminar ({DateTime.Now.ToString()})";
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarEstudiantes();
        }

        protected void gvEstudiantes_Sorting(object sender, GridViewSortEventArgs e)
        {
            _ordenarPor = e.SortExpression;
            ViewState["OrdenarPor"] = _ordenarPor;

            if (ViewState["TipoOrdenamiento"] == null)
            {
                _tipoOrdenamiento = "ASC";
                ViewState["TipoOrdenamiento"] = "ASC";
            }
            else
            {
                _tipoOrdenamiento = ViewState["TipoOrdenamiento"].ToString() == "ASC" ? "DESC" : "ASC";
                ViewState["TipoOrdenamiento"] = _tipoOrdenamiento;
            }

            CargarEstudiantes();
        }
        #endregion

        #region [ Métodos privados ]
        private void CargarCiudades()
        {
            ddlCiudadNacimiento.DataSource = _fachadaCiudad.RetornarCiudades();
            ddlCiudadNacimiento.DataValueField = "Id";
            ddlCiudadNacimiento.DataTextField = "NombreCiudad";
            ddlCiudadNacimiento.DataBind();

            ddlCiudadNacimiento.Items.Insert(0, new ListItem("-- Seleccione una ciudad --", ""));
        }

        private void CargarEstudiantes()
        {
            gvEstudiantes.DataSource = _fachadaEstudiante.RetornarEstudiantesDTO(txtNombre.Text, txtApellidos.Text, ddlCiudadNacimiento.SelectedValue, _ordenarPor, _tipoOrdenamiento);
            gvEstudiantes.DataBind();
        }
        #endregion

        
    }
}