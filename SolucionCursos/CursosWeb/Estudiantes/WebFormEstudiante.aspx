﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormEstudiante.aspx.cs" Inherits="CursosWeb.Estudiantes.WebFormEstudiante" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HyperLink ID="hlIngles" runat="server" Text="<%$Resources:idioma, lang %>" NavigateUrl="?lang=es" />
    <asp:HyperLink ID="hlEspanol" runat="server" Text="<%$Resources:idioma, lang %>" NavigateUrl="?lang=en" />
    
    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Estudiante
            </div>
            <div class="card-body">
                <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="false" CssClass="alert alert-danger"/>

                <%--Cedula--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCedula">
                        <asp:Literal Text="<%$Resources:idioma, cedula%>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtCedula" runat="server" CssClass="form-control" Placeholder="Ingrese la cédula"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCedula" runat="server" ErrorMessage="La cédula es requerida" ControlToValidate="txtCedula" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:CompareValidator ID="cvCedula" runat="server" ErrorMessage="La cédula debe ser un número entero" ControlToValidate="txtCedula"  Operator="DataTypeCheck" Type="Integer" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"/>
                    </div>
                </div>
                
                <%--Nombre--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtNombre">
                        <asp:Literal Text="<%$Resources:idioma, nombre%>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Placeholder="Ingrese el nombre" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ID="refNombre" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtNombre" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <%--Apellidos--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtApellido">
                        <asp:Literal Text="<%$Resources:idioma, apellido%>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtApellido" runat="server" CssClass="form-control" Placeholder="Ingrese el apellido"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revApellidos" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtApellido" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <%--Correo--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCorreo">Correo:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control" Placeholder="Ingrese el correo"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revCorreo" runat="server" ErrorMessage="El correo no es válido" ControlToValidate="txtCorreo" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <%--Genero--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Género:</label>
                    <div class="col-md-10">
                        <asp:RadioButtonList ID="rdoGenero" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Femenino" Value="false" Selected="True"/>
                            <asp:ListItem Text="Masculino" Value="true"/>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <%--Ciudad de Nacimiento--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Ciudad:</label>
                    <div class="col-md-10">
                        <asp:DropDownList ID="ddlCiudadNacimiento" runat="server" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="La ciudad de nacimiento es requerida" ControlToValidate="ddlCiudadNacimiento" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnGuardar" runat="server" Text="Insertar" CssClass="btn btn-dark" OnClick="btnGuardar_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
