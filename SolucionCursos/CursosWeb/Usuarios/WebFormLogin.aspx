﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormLogin.aspx.cs" Inherits="CursosWeb.Usuarios.WebFormLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Inicio de sesión
            </div>
            <div class="card-body">

                <asp:PlaceHolder ID="phEstadoLogin" runat="server" Visible="false">
                    <p>
                        <asp:Literal ID="lblEstado" runat="server" />
                    </p>
                </asp:PlaceHolder>

                <asp:PlaceHolder ID="phFormularioLogin" runat="server">
                    <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="false" CssClass="alert alert-danger" />

                    <%--Usuario--%>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="txtUsuario">Usuario:</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" Placeholder="Ingrese el usuario" MaxLength="250"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ErrorMessage="El usuario es requerido" ControlToValidate="txtUsuario" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                            <asp:RegularExpressionValidator ID="revUsuario" runat="server" ErrorMessage="El usuario no es correo válido" ControlToValidate="txtUsuario" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        </div>
                    </div>

                    <%--Clave--%>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="txtClave">Clave:</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Placeholder="Ingrese la clave" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="rfvClave" runat="server" ErrorMessage="La clave es requerida" ControlToValidate="txtClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        </div>
                    </div>

                    <%--Botón iniciar sesión--%>
                    <div class="form-group row">
                        <div class="offset-md-2 col-md-10">
                            <asp:Button ID="btnIniciarSesion" runat="server" Text="Iniciar sesión" CssClass="btn btn-dark" OnClick="btnIniciarSesion_Click" />
                        </div>
                    </div>

                    <%--Label de información--%>
                    <div class="form-group row">
                        <div class="offset-md-2 col-md-10">
                            <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                        </div>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder ID="phCerrarSesion" runat="server" Visible="false">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Button ID="btnCerrarSesion" runat="server" Text="Cerrar sesión" CssClass="btn btn-dark" OnClick="btnCerrarSesion_Click" />
                        </div>
                    </div>
                </asp:PlaceHolder>

            </div>
        </div>
    </div>

</asp:Content>
