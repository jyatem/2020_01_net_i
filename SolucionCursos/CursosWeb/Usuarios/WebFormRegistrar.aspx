﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormRegistrar.aspx.cs" Inherits="CursosWeb.Usuarios.WebFormRegistrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Registrar Usuario
            </div>
            <div class="card-body">
                <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="false" CssClass="alert alert-danger"/>

                <%--Usuario--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtUsuario">Usuario:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" Placeholder="Ingrese el usuario" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ErrorMessage="El usuario es requerido" ControlToValidate="txtUsuario" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ID="revUsuario" runat="server" ErrorMessage="El usuario no es correo válido" ControlToValidate="txtUsuario" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <%--Clave--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtClave">Clave:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Placeholder="Ingrese la clave" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="rfvClave" runat="server" ErrorMessage="La clave es requerida" ControlToValidate="txtClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <%--Confirmar Clave--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtConfirmarClave">Confirmar Clave:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtConfirmarClave" runat="server" CssClass="form-control" Placeholder="Confirme la clave" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="rfvConfirmarClave" runat="server" ErrorMessage="Confirme la clave" ControlToValidate="txtConfirmarClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:CompareValidator ID="cvConfirmarClave" runat="server" ErrorMessage="Las claves no coincides" ControlToValidate="txtConfirmarClave" ControlToCompare="txtClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" CssClass="btn btn-dark" OnClick="btnRegistrar_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
