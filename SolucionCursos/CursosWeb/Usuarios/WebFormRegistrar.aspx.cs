﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Usuarios
{
    public partial class WebFormRegistrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var almacenamientoUsuarios = new UserStore<IdentityUser>();
                    var administrar = new UserManager<IdentityUser>(almacenamientoUsuarios);
                    var usuario = new IdentityUser { UserName = txtUsuario.Text };

                    IdentityResult resultado = administrar.Create(usuario, txtClave.Text);

                    if (resultado.Succeeded)
                    {
                        //lblInformacion.CssClass = "text-success";
                        //lblInformacion.Text = $"El usuario {usuario.UserName} fue creado exitosamente";

                        var adminAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
                        var identidadUsuario = administrar.CreateIdentity(usuario, DefaultAuthenticationTypes.ApplicationCookie);

                        adminAutenticacion.SignIn(new AuthenticationProperties() { IsPersistent = false }, identidadUsuario);

                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        lblInformacion.CssClass = "text-danger";
                        lblInformacion.Text = resultado.Errors.FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }
    }
}