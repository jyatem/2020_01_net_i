﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace CursosWeb
{
    public class BasePage : Page
    {
        protected override void OnLoadComplete(EventArgs e)
        {
            // Seguridad
            base.OnLoadComplete(e);
        }

        protected override void InitializeCulture()
        {
            string culture = string.Empty;

            if (!String.IsNullOrEmpty(Request["lang"]))
            {
                string lang = Request["lang"].ToLower();

                Session["lang"] = lang;

                switch (lang)
                {
                    case "en":
                        culture = "en-US";
                        break;
                    case "es":
                        culture = "es-CO";
                        break;
                    default:
                        culture = "es-CO";
                        break;
                }
            }
            else
            {
                culture = "es-CO";
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            
            base.InitializeCulture();
        }
    }
}