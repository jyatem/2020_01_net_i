﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Departamentos
{
    public partial class WebFormListarDepartamentos : System.Web.UI.Page
    {
        #region [ Variables privadas ]
        private FachadaDepartamento _fachadaDepartamento = new FachadaDepartamento();
        #endregion

        #region [ Eventos de la página ]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarDepartamentos();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarDepartamentos();
        }

        protected void gvDepartamentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDepartamentos.PageIndex = e.NewPageIndex;
            CargarDepartamentos();
        }

        protected void gvDepartamentos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Eliminar")
            {
                int id = Convert.ToInt32(e.CommandArgument);

                if (_fachadaDepartamento.EliminarDepartamento(id) == 1)
                {
                    CargarDepartamentos();
                    lblInformacion.CssClass = "text-success";
                    lblInformacion.Text = $"El departamento se eliminó correctamente ({DateTime.Now.ToString()})";
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = $"El departamento no se pudo eliminar ({DateTime.Now.ToString()})";
                }
            }
        }
        #endregion

        #region [ Métodos privados ]
        private void CargarDepartamentos()
        {
            gvDepartamentos.DataSource = _fachadaDepartamento.RetornarDepartamentos(txtNombre.Text);
            gvDepartamentos.DataBind();
        }
        #endregion
    }
}