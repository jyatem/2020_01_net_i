﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormListarDepartamentos.aspx.cs" Inherits="CursosWeb.Departamentos.WebFormListarDepartamentos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <link href="../Content/paginacion.css" rel="stylesheet" />

    <div class="form-inline mt-3">
        <div class="form-group">
            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control mr-2" placeholder="Nombre del departamento"></asp:TextBox>
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click"/>
        </div>
    </div>
    
    <asp:GridView ID="gvDepartamentos" runat="server" CssClass="table table-striped table-dark table-hover mt-3" AutoGenerateColumns="false" AllowPaging="true" PageSize="2" OnPageIndexChanging="gvDepartamentos_PageIndexChanging" OnRowCommand="gvDepartamentos_RowCommand" EmptyDataText="No existen departamentos" AllowSorting="true">
        <Columns>
            <asp:HyperLinkField DataTextField="Id" HeaderText="Id" DataNavigateUrlFormatString="~/Departamentos/WebFormDepartamento.aspx?Id={0}" DataNavigateUrlFields="Id" />
            <asp:BoundField HeaderText="Nombre" DataField="NombreDepartamento"/>
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClientClick="return mensajeEliminar(this.name, '¿Realmente desea eliminar este departamento?');" CommandName="Eliminar" CommandArgument='<%# Bind("Id") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>

    <script src="../Scripts/mensajescursosweb.js"></script>

</asp:Content>
