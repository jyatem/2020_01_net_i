﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormDepartamento.aspx.cs" Inherits="CursosWeb.Departamentos.WebFormDepartamento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Departamento
            </div>
            <div class="card-body">
                <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="false" CssClass="alert alert-danger"/>

                <%--Id--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtId">Id:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Placeholder="Se autoincrementa" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                
                <%--Nombre--%>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtNombre">Nombre:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Placeholder="Ingrese el nombre" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ID="refNombre" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtNombre" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnGuardar" runat="server" Text="Insertar" CssClass="btn btn-dark" OnClick="btnGuardar_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
