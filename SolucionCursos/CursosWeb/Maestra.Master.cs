﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb
{
    public partial class Maestra : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lgsCerrarSesion_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            var adminAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
            adminAutenticacion.SignOut();
            Response.Redirect("~/Default.aspx");
        }
    }
}