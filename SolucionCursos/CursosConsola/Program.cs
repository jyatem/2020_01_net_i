﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursosConsola
{
    class Program
    {
        static void Main(string[] args)
        {
			try
			{
				FachadaEstudiante fachadaEstudiante = new FachadaEstudiante();

                #region [ Insertar Estudiante ]
                //Estudiante estudiante = new Estudiante
                //{
                //	Cedula = 80,
                //	Nombre = "Jairo",
                //	Apellidos = "Yate",
                //	CiudadNacimiento = 6,
                //	Correo = "jyatem@yahoo.com",
                //	Genero = true
                //};

                //int cantidadRegistros = fachadaEstudiante.InsertarEstudiante(estudiante);

                //if (cantidadRegistros == 1)
                //{
                //	Console.WriteLine("Estudiante ingresado exitosamente");
                //}
                //else
                //{
                //	Console.WriteLine("El estudiante no se pudo ingresar");
                //}
                #endregion

                #region [ Obtener un estudiante ]
                //Estudiante estudiante = fachadaEstudiante.RetornarEstudiante(80);

                //if (estudiante != null)
                //{
                //    Console.WriteLine($"{estudiante.Nombre} {estudiante.Apellidos}");
                //}
                //else
                //{
                //    Console.WriteLine("El estudiante no existe");
                //}
                #endregion

                #region [ Retornar todos los estudiantes ]
                foreach (Estudiante estudiante in fachadaEstudiante.RetornarEstudiantes())
                {
                    Console.WriteLine($"Nombre: {estudiante.Nombre} {estudiante.Apellidos} - Ciudad de nacimiento: {estudiante.Ciudad.NombreCiudad}");
                }
                #endregion
            }
            catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				Console.ReadLine();
			}
        }
    }
}
