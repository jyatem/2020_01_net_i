﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.DTOs
{
    public class CiudadDTO
    {
        public int Id { get; set; }

        public string NombreCiudad { get; set; }

        public string NombreDepartamento { get; set; }
    }
}
