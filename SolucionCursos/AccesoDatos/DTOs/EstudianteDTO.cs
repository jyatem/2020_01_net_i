﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.DTOs
{
    public class EstudianteDTO
    {
        public int Cedula { get; set; }

        public string NombreCompleto { get; set; }

        public string NombreCiudadNacimiento { get; set; }

        public string Genero { get; set; }

        public int IdCiudadNacimiento { get; set; }
    }
}
