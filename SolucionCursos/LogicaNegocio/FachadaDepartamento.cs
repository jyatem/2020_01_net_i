﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaDepartamento
    {
        private DBCursosEntities _contexto;

        public FachadaDepartamento()
        {
            _contexto = new DBCursosEntities();
        }

        public int InsertarDepartamento(Departamento departamento)
        {
            int cantidad = _contexto.Departamentoes.Where(d => d.NombreDepartamento.ToLower() == departamento.NombreDepartamento.ToLower()).Count();

            if (cantidad != 0)
            {
                throw new Exception("El departamento ya existe");
            }
            
            _contexto.Departamentoes.Add(departamento);
            return _contexto.SaveChanges();
        }

        public Departamento RetornarDepartamento(int id)
        {
            return _contexto.Departamentoes.Find(id);
        }

        public List<Departamento> RetornarDepartamentos(string nombre)
        {
            var departamentos = from d in _contexto.Departamentoes
                                select d;

            if (!String.IsNullOrEmpty(nombre))
            {
                departamentos = departamentos.Where(d => d.NombreDepartamento.Contains(nombre));
            }

            return departamentos.ToList();
        }

        public int EliminarDepartamento(int id)
        {
            Departamento departamento = _contexto.Departamentoes.Find(id);

            if (departamento != null)
            {
                _contexto.Departamentoes.Remove(departamento);
                return _contexto.SaveChanges();
            }

            return 0;
        }

        public int ActualizarDepartamento(Departamento departamento)
        {
            _contexto.Entry(departamento).State = EntityState.Modified;
            return _contexto.SaveChanges();
        }
    }
}
