﻿using AccesoDatos;
using AccesoDatos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaCiudad
    {
        private DBCursosEntities _contexto;

        public FachadaCiudad()
        {
            _contexto = new DBCursosEntities();
        }

        public List<Ciudad> RetornarCiudades()
        {
            return _contexto.Ciudads.OrderBy(c => c.NombreCiudad).ToList();
        }

        //public int InsertarCiudad(Ciudad ciudad)
        //{
        //    //List<Ciudad> ciudadesExistentes = _contexto.Ciudads.Where(c => c.NombreCiudad.ToLower() == ciudad.NombreCiudad.ToLower()).ToList();

        //    Ciudad ciudadExiste = _contexto.Ciudads.FirstOrDefault(c => c.NombreCiudad.ToLower() == ciudad.NombreCiudad.ToLower());

        //    if(ciudadExiste != null)
        //    {
        //        throw new Exception("La ciudad ya existe");
        //    }

        //    return 0;
        //}

        public List<CiudadDTO> RetornarCiudadesDTO()
        {
            return _contexto.Ciudads.OrderBy(c => c.NombreCiudad).Select(c => new CiudadDTO { Id = c.Id, NombreCiudad = c.NombreCiudad, NombreDepartamento = c.Departamento.NombreDepartamento }).ToList();
        }
    }
}
