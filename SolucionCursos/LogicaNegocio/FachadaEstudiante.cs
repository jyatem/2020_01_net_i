﻿using AccesoDatos;
using AccesoDatos.DTOs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaEstudiante
    {
        private DBCursosEntities _contexto;

        public FachadaEstudiante()
        {
            _contexto = new DBCursosEntities();
            _contexto.Database.Log = sql => Debug.Write(sql);
        }

        public int InsertarEstudiante(Estudiante estudiante)
        {
            if (String.IsNullOrEmpty(estudiante.Nombre))
            {
                throw new Exception("El nombre no puede ser vacio o nulo");
            }

            _contexto.Estudiantes.Add(estudiante);
            return _contexto.SaveChanges();
        }

        public Estudiante RetornarEstudiante(int cedula)
        {
            return _contexto.Estudiantes.Find(cedula);
        }

        public List<Estudiante> RetornarEstudiantes()
        {
            return _contexto.Estudiantes.OrderBy(e => e.Nombre).ToList();

            //return _contexto.Estudiantes.Where(e => e.Cedula == 80).ToList();

            //return _contexto.Estudiantes.Where(e => e.Nombre.Contains("j")).ToList();

            //return _contexto.Estudiantes.Where(e => e.Nombre != "Jairo").ToList();

            //return _contexto.Estudiantes.Where(e => !e.Nombre.Contains("Jairo")).ToList();

            //return _contexto.Estudiantes.Where(e => e.Nombre == "Jairo" && e.Apellidos == "Yate").ToList();
        }

        public List<EstudianteDTO> RetornarEstudiantesDTO(string nombre, string apellidos, string idCiudad, string ordenarPor, string tipoOrdenamiento)
        {
            var estudiantes = from e in _contexto.Estudiantes
                              select new EstudianteDTO
                              {
                                  Cedula = e.Cedula,
                                  NombreCompleto = e.Nombre + " " + e.Apellidos,
                                  NombreCiudadNacimiento = e.Ciudad.NombreCiudad,
                                  Genero = e.Genero ? "Masculino" : "Femenino",
                                  IdCiudadNacimiento = e.CiudadNacimiento
                              };

            if (!String.IsNullOrEmpty(nombre))
            {
                estudiantes = estudiantes.Where(e => e.NombreCompleto.Contains(nombre));
            }

            if (!String.IsNullOrEmpty(apellidos))
            {
                estudiantes = estudiantes.Where(e => e.NombreCompleto.Contains(apellidos));
            }

            if (!String.IsNullOrEmpty(idCiudad))
            {
                int valorCiudad = Convert.ToInt32(idCiudad);
                estudiantes = estudiantes.Where(e => e.IdCiudadNacimiento == valorCiudad);
            }

            if (!String.IsNullOrEmpty(ordenarPor))
            {
                if (ordenarPor == "Nombre")
                {
                    if (tipoOrdenamiento == "ASC")
                    {
                        estudiantes = estudiantes.OrderBy(e => e.NombreCompleto);
                    }
                    else if (tipoOrdenamiento == "DESC")
                    {
                        estudiantes = estudiantes.OrderByDescending(e => e.NombreCompleto);
                    }
                }
            }

            return estudiantes.ToList();
        }

        public int EliminarEstudiante(int cedula)
        {
            Estudiante estudiante = _contexto.Estudiantes.Find(cedula);

            if (estudiante != null)
            {
                _contexto.Estudiantes.Remove(estudiante);
                return _contexto.SaveChanges();
            }

            return 0;
        }

        public int ActualizarEstudiante(Estudiante estudiante)
        {
            _contexto.Entry(estudiante).State = EntityState.Modified;
            return _contexto.SaveChanges();
        }
    }
}
