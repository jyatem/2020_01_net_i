﻿using AccesoDatos.DTOs;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServicioWebCursos
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ServicioCurso : IServicioCurso
    {
        public List<CiudadDTO> RetornarCiudades()
        {
            FachadaCiudad fachadaCiudad = new FachadaCiudad();
            return fachadaCiudad.RetornarCiudadesDTO();
        }

        public int Sumar(int valor1, int valor2)
        {
            return valor1 + valor2;
        }
    }
}
