﻿using AccesoDatos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServicioWebCursos
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IServicioCurso
    {
        [OperationContract]
        int Sumar(int valor1, int valor2);

        [OperationContract]
        List<CiudadDTO> RetornarCiudades();
    }
}
