﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Padre padre = new Padre();

            string valor = padre.Propiedad1Padre;
            padre.MetodoPadre(10);
            
            Hija hija = new Hija();
            int intValor = hija.PropiedadHija;
            hija.MetodoHija();
            
        }
    }
}
