﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Herencia
{
    public class Hija : Padre
    {
        public int PropiedadHija { get; set; }

        public Hija()
        {
            
        }

        public void MetodoHija()
        {
            Console.WriteLine("Método de la clase hija");
        }
    }
}
