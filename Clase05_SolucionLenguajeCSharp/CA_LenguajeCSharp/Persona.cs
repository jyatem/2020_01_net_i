﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LenguajeCSharp
{
    public class Persona
    {
        #region [ Propiedades ]
        public string Nombre { get; set; }

        public int Edad
        {
            get
            {
                // Implementar un código que calcule la edad usando
                // la fecha de nacimiento
                int edad = DateTime.Today.AddTicks(-FechaNacimiento.Ticks).Year - 1;
                return edad;
            }
        }

        public DateTime FechaNacimiento { get; set; }

        public bool? Casado { get; set; }

        public Genero GeneroPersona { get; set; }

        // Es un atributo
        //public int _valor; 
        #endregion

        #region [ Constructores ]
        public Persona()
        {

        }

        public Persona(string nombre, int edad, DateTime fechaNacimiento)
        {
            Nombre = nombre;
            //Edad = edad;
            FechaNacimiento = fechaNacimiento;
        }

        public Persona(int mes, string nombre, int dia, int ahno)
        {
            Nombre = nombre;
            FechaNacimiento = new DateTime(ahno, mes, dia);
        }
        #endregion

        #region [ Métodos ]
        public void MostrarInformacion()
        {
            // Jairo tiene 39 años, nació en 1980-8-13, es Masculino y está casado(a)
            Console.WriteLine("{0} tiene {1} años, nació en {2}, es {3} y {4}", Nombre, Edad, FechaNacimiento.ToString("yyyy-MM-dd"), GeneroPersona, Casado.Value ? "está casado(a)" : "no esta casado(a)");
        }

        public bool EsMayorDeEdad()
        {
            return Edad >= 18;
        }
        #endregion
    }
}
