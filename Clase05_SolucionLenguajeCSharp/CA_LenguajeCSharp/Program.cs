﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LenguajeCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //string Nombre = "Empresa X";

            #region [ Instanciando clase con constructor sin parametros ]
            Persona persona1 = new Persona();
            persona1.Nombre = "Jairo";
            //persona1.Edad = 39;
            persona1.Casado = true;
            persona1.GeneroPersona = Genero.Masculino;
            persona1.FechaNacimiento = new DateTime(1980, 8, 13);
            #endregion

            #region [ Instanciando clase con llaves ]
            Persona persona2 = new Persona
            {
                Nombre = "Lina",
                //Edad = 40,
                Casado = true,
                GeneroPersona = Genero.Femenino,
                FechaNacimiento = Convert.ToDateTime("1979-12-29")
            };
            #endregion

            #region [ Mostrar información con +, con {} y con $ ]
            Console.WriteLine("Nombre: " + persona1.Nombre + " Edad: " + persona1.Edad + " Fecha de Nacimiento: " + persona1.FechaNacimiento);

            Console.WriteLine("Nombre: {0} Edad: {1} Fecha de Nacimiento: {2}", persona2.Nombre, persona2.Edad, persona2.FechaNacimiento);

            Console.WriteLine($"Nombre: {persona1.Nombre} Edad: {persona1.Edad} Fecha de Nacimiento: {persona1.FechaNacimiento}");
            #endregion

            #region [ Instanciando con el constructor con 3 parametros ]
            Persona persona3 = new Persona("Mariana", 14, new DateTime(2006, 2, 14));
            persona3.Casado = false;
            persona3.GeneroPersona = Genero.Femenino;

            Console.WriteLine($"Nombre: {persona3.Nombre} Edad: {persona3.Edad} Fecha de Nacimiento: {persona3.FechaNacimiento}");

            //Fecha del sistema
            // DateTime.Now
            #endregion

            #region [ Instanciando con el constructor de 4 parametros ]
            Persona persona4 = new Persona(7, "Manuel", 24, 2010);
            persona4.Casado = false;
            persona4.GeneroPersona = Genero.Masculino;

            Console.WriteLine($"Nombre: {persona4.Nombre} Edad: {persona4.Edad} Fecha de Nacimiento: {persona4.FechaNacimiento}");

            // Cómo validar un Nullable
            if (persona4.Casado.HasValue)
            {
                Console.WriteLine("Casado tiene valor y es: " + persona4.Casado);
            }
            else
            {
                Console.WriteLine("Casado es null");
            }
            #endregion

            #region [ Nullable y valores null ]
            int? x = null;
            double? y = null;

            x = 2;

            Persona persona5 = null;
            #endregion

            #region [ Invocar MostrarInformacion ]
            // Ejemplo
            // Jairo tiene 39 años, nació en 1980-8-13, es Masculino y está casado(a)
            Console.WriteLine();
            Console.WriteLine("*************************************************************");
            //persona3.MostrarInformacion(); 
            #endregion

            #region [ Es mayor de edad ]
            if (persona3.EsMayorDeEdad())
            {
                Console.WriteLine($"{persona3.Nombre} es mayor de edad");
            }
            else
            {
                Console.WriteLine($"{persona3.Nombre} no es mayor de edad");
            }
            #endregion

            #region [ Generics ]
            // Generics estructura de datos
            int[] arregloEnteros = new int[2];
            arregloEnteros[0] = 13;
            arregloEnteros[1] = 200;

            Console.WriteLine();
            for (int i = 0; i < arregloEnteros.Length; i++)
            {
                Console.WriteLine($"Posición {i} = {arregloEnteros[i]}");
            }

            List<int> listadoEnteros = new List<int>();

            listadoEnteros.Add(1);
            listadoEnteros.Add(-20);
            listadoEnteros.Add(140);
            listadoEnteros.Add(10);
            listadoEnteros.Add(0);
            listadoEnteros.Add(-8);

            Console.WriteLine();
            Console.WriteLine("Generics");
            foreach (int item in listadoEnteros)
            {
                Console.WriteLine($"Valor: {item}");
            }

            List<string> listadoNombres = new List<string>();

            listadoNombres.Add("Jairo");
            listadoNombres.Add("Lina");
            listadoNombres.Add("Mariana");
            listadoNombres.Add("Manuel");

            Console.WriteLine();
            Console.WriteLine("Nombres");
            foreach (string nombre in listadoNombres)
            {
                Console.WriteLine(nombre);
            }

            List<string> otroListado = new List<string> { "Eduard", "Mauricio", "Juan", "Deivi" };

            Console.WriteLine();
            Console.WriteLine("Listado inicializado");
            foreach (string nombre in otroListado)
            {
                Console.WriteLine(nombre);
            }

            // En el foreach invocar el método MostrarInformacion
            List<Persona> personas = new List<Persona>();

            personas.Add(persona1);
            personas.Add(persona2);
            personas.Add(persona3);
            personas.Add(persona4);
            personas.Add(new Persona { Nombre = "Raquel", Casado = true, FechaNacimiento = new DateTime(1969, 9, 14), GeneroPersona = Genero.Femenino });

            Console.WriteLine();
            Console.WriteLine("Listado de personas");
            foreach (Persona persona in personas)
            {
                persona.MostrarInformacion();
            }

            #endregion

            Console.ReadLine();
        }
    }
}
