﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_MetodosEstaticos
{
    class Program
    {
        static void Main(string[] args)
        {
            int resultado = Calculadora.Sumar(10, 13);
            Console.WriteLine(resultado);

            // Crear un método que determine si un número es par o impar. EsPar. Listado de número enteros positivos. Recorre y van a mostrar en consola el número y van a colocar si es par o impar.
            List<int> numeros = new List<int> { 2, 5, 13, 80, 121 };

            foreach (int numero in numeros)
            {
                Console.WriteLine("El número {0} es {1}", numero, Calculadora.EsPar(numero) ? "par": "impar");
            }

            Console.ReadLine();
        }
    }
}
