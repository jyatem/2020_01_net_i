﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_ConversionStringMath
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [ Convertir String a Número ]
            string strNumero = "123";

            //int numero1 = int.Parse(strNumero);

            int numero1;
            bool esNumero = int.TryParse(strNumero, out numero1);

            if (esNumero)
            {
                Console.WriteLine(numero1);
            }
            else
            {
                Console.WriteLine($"{strNumero} no es número válido");
            }

            //double.TryParse
            //bool.TryParse
            //float.TryParse

            int numero2 = Convert.ToInt32("454");

            string strObtenerValor = numero2.ToString();

            #endregion

            #region [ Funciones de la clase String ]
            string frase = "Frase de prueba";

            // Retorna la longitud de la cadena
            int longitud = frase.Length;

            // Si quiero saber si contiene una palabra
            bool siContiene = frase.Contains("se");

            // Volver minúscula
            string minuscula = frase.ToLower();

            // Volver mayúscula
            string mayuscula = frase.ToUpper();

            // Si quiero saber la primer posición de un caracter
            int posicion = frase.IndexOf('a');

            // Si quiero saber la última posición de un caracter
            int ultimaPosicion = frase.LastIndexOf('a');

            // Cortar parte de la cadena
            string corte = frase.Remove(5);

            string corte2 = frase.Remove(6, 2);

            // Quitar espacios
            string conEspacios = "      contenido     ";
            string sinEspacios = conEspacios.Trim();

            // Quitar espacios al principio
            string sinEspaciosAlPrincipio = conEspacios.TrimStart();

            // Quitar espacios al final
            string sinEspaciosAlFinal = conEspacios.TrimEnd();

            // Reemplazar un caracter por otro
            string nueva = frase.Replace('a', '*');

            // Obtener un pedazo de la cadena
            string trozo = frase.Substring(6, 2);

            // Formatear una fecha
            Console.WriteLine("{0:yyyy-MMMM-dd}", DateTime.Now);

            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd"));

            // Formatear número
            Console.WriteLine(String.Format("{0:0,0#}", 12.46));
            #endregion

            #region [ Funciones de la clase Math ]
            // Redondeo hacia arriba
            double resultadoArriba = Math.Ceiling(5.47);

            // Redondeo hacia abajo
            double resultadoAbajo = Math.Floor(5.44);

            // Redonde General
            double resultadoRedondeo = Math.Round(5.4);
            resultadoRedondeo = Math.Round(5.6);

            // Potencia
            double potencia = Math.Pow(2, 3);

            // Raíz cuadrada
            double raizCuadrada = Math.Sqrt(9);

            // Truncar valor
            double truncar = Math.Truncate(7.234);
            #endregion

            #region [ Ejercicio con métodos String ]
            string mensaje1 = "La controladora XYZABCD no encontró la ruta '/paginaX' o no ha sido implementada";

            string mensaje2 = "La controladora XYZABCDDDFGDSG no encontró la ruta '/paginaOtraXY' o no ha sido implementada";

            Console.WriteLine(ObtenerNombre(mensaje1));
            Console.WriteLine(ObtenerNombre(mensaje2));

            // Resultado
            // paginaX
            // paginaOtraXY

            #endregion

            Console.ReadLine();
        }

        private static string ObtenerNombre(string mensaje)
        {
            return mensaje.Substring(mensaje.IndexOf('/') + 1, mensaje.LastIndexOf("'") - mensaje.IndexOf("/") - 1);
        }
    }
}
