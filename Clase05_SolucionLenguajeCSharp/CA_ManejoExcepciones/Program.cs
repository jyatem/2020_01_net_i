﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_ManejoExcepciones
{
    class Program
    {
        static void Main(string[] args)
        {
			try
			{
				//int i = 0;
				//int j = 10 / i;

				string dato = null;

				Funcion(dato);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			Console.ReadLine();
        }

		static void Funcion(string s)
		{
			if (String.IsNullOrEmpty(s))
			{
				throw new Exception("El valor es nulo o vacio");
			}
		}
    }
}
