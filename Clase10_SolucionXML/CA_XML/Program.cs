﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CA_XML
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(@"C:\Diplomado\NET_I\Clase10_SolucionXML\CA_XML\Empleados.xml");

            #region [ Consultar por país ]
            XmlNodeList empleadosPorPais = xmlDocument.SelectNodes("/Empleados/Empleado[Pais='Ecuador']");
            ImprimirInformacion(empleadosPorPais);
            #endregion

            #region [ Consultar por el atributo Ciudad ]
            Console.WriteLine();
            XmlNodeList empleadosPorCiudad = xmlDocument.SelectNodes("/Empleados/Empleado[@Ciudad='Medellin']");

            ImprimirInformacion(empleadosPorCiudad);
            #endregion

            #region [ Consultar por el atributo Genero ]
            Console.WriteLine();
            XmlNodeList empleadosPorGenero = xmlDocument.SelectNodes("/Empleados/Empleado[@Genero='M']");

            ImprimirInformacion(empleadosPorGenero);
            #endregion

            #region [ Consultar por el atributo Ciudad y Genero ]
            Console.WriteLine();
            XmlNodeList empleadosPorCiudadYGenero = xmlDocument.SelectNodes("/Empleados/Empleado[@Ciudad='Quito' and @Genero='M']");

            ImprimirInformacion(empleadosPorCiudadYGenero);
            #endregion

            #region [ Crear empleado en el XML ]
            XmlElement empleado = xmlDocument.CreateElement("Empleado");

            //Atributos
            XmlAttribute id = xmlDocument.CreateAttribute("Id");
            id.Value = "5";

            XmlAttribute ciudad = xmlDocument.CreateAttribute("Ciudad");
            ciudad.Value = "Itagui";

            XmlAttribute genero = xmlDocument.CreateAttribute("Genero");
            genero.Value = "F";

            empleado.Attributes.Append(id);
            empleado.Attributes.Append(ciudad);
            empleado.Attributes.Append(genero);

            // Elementos del empleado
            XmlElement nombre = xmlDocument.CreateElement("Nombre");
            nombre.InnerText = "Maria";

            XmlElement pais = xmlDocument.CreateElement("Pais");
            pais.InnerText = "Colombia";

            empleado.AppendChild(nombre);
            empleado.AppendChild(pais);

            xmlDocument.DocumentElement.AppendChild(empleado);
            xmlDocument.Save(@"C:\Diplomado\NET_I\Clase10_SolucionXML\CA_XML\EmpleadosConUnoNuevo.xml");
            #endregion

            Console.ReadLine();
        }

        private static void ImprimirInformacion(XmlNodeList listado)
        {
            foreach (XmlNode node in listado)
            {
                Console.WriteLine($"Nombre: {node["Nombre"].InnerText} de {node.Attributes["Ciudad"].Value}");
            }
        }
    }
}
