﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormEnviarCorreo.aspx.cs" Inherits="WA_Bootstrap.WebFormEnviarCorreo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Enviar Correo
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtPara">Para:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtPara" runat="server" CssClass="form-control" Placeholder="Ingrese el para"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtAsunto">Asunto:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtAsunto" runat="server" CssClass="form-control" Placeholder="Ingrese el asunto"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtMensaje">Mensaje:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtMensaje" runat="server" CssClass="form-control" Placeholder="Ingrese el mensaje" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="btn btn-dark" OnClick="btnEnviar_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
