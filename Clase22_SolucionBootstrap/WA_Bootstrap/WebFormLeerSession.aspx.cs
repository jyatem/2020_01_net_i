﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormLeerSession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLeer_Click(object sender, EventArgs e)
        {
            if (Session["Variable"] != null)
            {
                lblInformacion.Text = "Valor en session: " + Session["Variable"].ToString();
            }
            else
            {
                lblInformacion.Text = "La variable de session no existe";
            }

            //Session.Clear();
            //Session.RemoveAll();
        }
    }
}