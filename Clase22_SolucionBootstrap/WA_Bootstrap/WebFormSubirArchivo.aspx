﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormSubirArchivo.aspx.cs" Inherits="WA_Bootstrap.WebFormSubirArchivo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Formulario Subir Archivo
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Archivo:</label>
                    <div class="col-md-10">
                        <asp:FileUpload ID="fuArchivo" runat="server" CssClass="form-control"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnSubir" runat="server" Text="Subir" CssClass="btn btn-dark" OnClick="btnSubir_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
