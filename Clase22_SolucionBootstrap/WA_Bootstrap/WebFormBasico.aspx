﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormBasico.aspx.cs" Inherits="WA_Bootstrap.WebFormBasico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Formulario
            </div>
            <div class="card-body">
                <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger"/>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtNombre">Nombre:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Placeholder="Ingrese el nombre"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="true">&nbsp;</asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtApellido">Apellidos:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtApellido" runat="server" CssClass="form-control" Placeholder="Ingrese el apellido"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ErrorMessage="El apellido es requerido" ControlToValidate="txtApellido" Display="Dynamic" SetFocusOnError="true">&nbsp;</asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="btn btn-dark" OnClick="btnEnviar_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
