﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormLeerSession.aspx.cs" Inherits="WA_Bootstrap.WebFormLeerSession" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Leer variable de Session
            </div>
            <div class="card-body">

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnLeer" runat="server" Text="Leer" CssClass="btn btn-dark" OnClick="btnLeer_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
