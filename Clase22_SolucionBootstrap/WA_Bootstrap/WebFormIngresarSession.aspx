﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormIngresarSession.aspx.cs" Inherits="WA_Bootstrap.WebFormIngresarSession" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Ingresar variable de Session
            </div>
            <div class="card-body">

                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtValor">Valor de la variable de session:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtValor" runat="server" CssClass="form-control" Placeholder="Ingrese el valor"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-dark" OnClick="btnGuardar_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
