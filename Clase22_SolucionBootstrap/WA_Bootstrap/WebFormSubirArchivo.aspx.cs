﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormSubirArchivo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {
            if (fuArchivo.HasFile)
            {
                try
                {
                    string nombreArchivo = Path.GetFileName(fuArchivo.FileName);

                    fuArchivo.SaveAs(Server.MapPath("~/Archivos/") + nombreArchivo);

                    lblInformacion.CssClass = "text-success";
                    lblInformacion.Text = "Archivo subido exitosamente";
                }
                catch (Exception ex)
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = ex.Message;
                }
            }
        }
    }
}