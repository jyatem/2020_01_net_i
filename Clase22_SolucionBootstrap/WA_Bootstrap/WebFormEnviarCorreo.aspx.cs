﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormEnviarCorreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                smtpClient.Credentials = new NetworkCredential("pruebascedesistemasnetbasico@gmail.com", "Cedesistemas2019*");
                smtpClient.EnableSsl = true;

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = txtAsunto.Text;
                mailMessage.To.Add(new MailAddress(txtPara.Text));
                mailMessage.From = new MailAddress("pruebascedesistemasnetbasico@gmail.com", "Yate");
                mailMessage.IsBodyHtml = true;

                //mailMessage.Body = $"<h1 style='color:blue'>{txtMensaje.Text}</h1>";

                string mediaType = MediaTypeNames.Image.Jpeg;
                LinkedResource img = new LinkedResource(Server.MapPath("~/Archivos/Logo.jpg"), mediaType);
                img.ContentId = "imagen1";
                img.ContentType.MediaType = mediaType;
                img.TransferEncoding = TransferEncoding.Base64;
                img.ContentType.Name = img.ContentId;
                img.ContentLink = new Uri("cid:" + img.ContentId);

                string htmlMensaje = $"<h1 style='color:blue'>{txtMensaje.Text}</h1><img src='cid:imagen1' />";

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlMensaje, Encoding.UTF8, MediaTypeNames.Text.Html);

                htmlView.LinkedResources.Add(img);

                mailMessage.AlternateViews.Add(htmlView);

                #region [ Adjuntos ]
                //string archivo = Server.MapPath("~/Archivos/Ejercicio RPA.docx");

                //Attachment attachment = new Attachment(archivo);

                //mailMessage.Attachments.Add(attachment); 
                #endregion

                smtpClient.Send(mailMessage);

                lblInformacion.CssClass = "text-success";
                lblInformacion.Text = "Correo enviado exitosamente";
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }
    }
}