﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace WA_Bootstrap.Utilidades
{
    public class Log
    {
        private string _encabezado;
        private string _fechaError;

        public Log()
        {
            _encabezado = DateTime.Now.ToString() + " ==>";
            _fechaError = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public void ErrorLog(string nombreRuta, string mensajeError)
        {
            StreamWriter streamWriter = new StreamWriter(nombreRuta + _fechaError + ".txt", true);
            streamWriter.WriteLine(_encabezado + mensajeError);
            streamWriter.Flush();
            streamWriter.Close();
        }

        public void ErrorLog(string mensajeError)
        {
            string nombreRuta = ConfigurationManager.AppSettings["RutaLog"];

            StreamWriter streamWriter = new StreamWriter(nombreRuta + _fechaError + ".txt", true);
            streamWriter.WriteLine(_encabezado + mensajeError);
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}