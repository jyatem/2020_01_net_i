﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormIsPostBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlPais.Items.Add(new ListItem("-- Seleccione --"));
                ddlPais.Items.Add(new ListItem("Colombia"));
                ddlPais.Items.Add(new ListItem("Ecuador"));
                ddlPais.Items.Add(new ListItem("Perú"));
            }
        }

        protected void btnHacerPeticion_Click(object sender, EventArgs e)
        {
            lblInformacion.Text = $"{ddlPais.SelectedValue} -> {ddlDepartamento.SelectedValue} -> {ddlCiudad.SelectedValue}";
        }

        protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDepartamento.Items.Clear();
            ddlCiudad.Items.Clear();
            ddlDepartamento.Items.Add(new ListItem("-- Seleccione --"));

            switch (ddlPais.SelectedValue)
            {
                case "Colombia":
                    ddlDepartamento.Items.Add(new ListItem("Antioquia"));
                    ddlDepartamento.Items.Add(new ListItem("Valle del cauca"));
                    break;
                case "Ecuador":
                    ddlDepartamento.Items.Add(new ListItem("Guayaquil"));
                    ddlDepartamento.Items.Add(new ListItem("Chaco"));
                    break;
                case "Perú":
                    ddlDepartamento.Items.Add(new ListItem("Cusco"));
                    break;
                default:
                    break;
            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCiudad.Items.Clear();
            ddlCiudad.Items.Add(new ListItem("-- Seleccione --"));

            switch (ddlDepartamento.SelectedValue)
            {
                case "Antioquia":
                    ddlCiudad.Items.Add(new ListItem("Medellin"));
                    ddlCiudad.Items.Add(new ListItem("Itagüí"));
                    break;
                case "Valle del cauca":
                    ddlCiudad.Items.Add(new ListItem("Cali"));
                    ddlCiudad.Items.Add(new ListItem("Tuluá"));
                    break;
                default:
                    break;
            }
        }
    }
}