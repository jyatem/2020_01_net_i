﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormCookies.aspx.cs" Inherits="WA_Bootstrap.WebFormCookies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Cookie
            </div>
            <div class="card-body">

                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCookie">Valor del Cookie:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtCookie" runat="server" CssClass="form-control" Placeholder="Ingrese el valor del cookie"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-dark" OnClick="btnGuardar_Click"/>
                        <asp:Button ID="btnLeer" runat="server" Text="Leer" CssClass="btn btn-dark" OnClick="btnLeer_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
