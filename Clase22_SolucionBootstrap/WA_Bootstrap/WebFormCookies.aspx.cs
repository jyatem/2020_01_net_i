﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormCookies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            HttpCookie httpCookie = new HttpCookie("MiCookie");

            httpCookie.Value = txtCookie.Text;
            httpCookie.Expires = DateTime.Now.AddSeconds(120);

            Response.Cookies.Add(httpCookie);
            lblInformacion.Text = "La cookie se ha ingresado exitosamente";
        }

        protected void btnLeer_Click(object sender, EventArgs e)
        {
            HttpCookie httpCookie = Request.Cookies["MiCookie"];

            if (httpCookie != null)
            {
                txtCookie.Text = httpCookie.Value;
            }
            else
            {
                lblInformacion.Text = "La cookie no existe";
            }
        }
    }
}