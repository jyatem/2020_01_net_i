﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            MiClase miClase = new MiClase();
            miClase.MetodoDeLaInterfaz();
            Console.WriteLine(miClase.DuplicarValor(10));

            Console.WriteLine();
            
            IMiInterfaz desdeInterfaz = new MiClase();
            desdeInterfaz.MetodoDeLaInterfaz();

            Console.ReadLine();
        }
    }
}
