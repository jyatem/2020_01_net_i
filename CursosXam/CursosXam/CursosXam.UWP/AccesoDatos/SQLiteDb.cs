﻿using CursosXam.AccesoDatos;
using CursosXam.UWP.AccesoDatos;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(SQLiteDb))]
namespace CursosXam.UWP.AccesoDatos
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteAsyncConnection ObtenerConexion()
        {
            var ruta = Path.Combine(ApplicationData.Current.LocalFolder.Path, "CursosXam.db3");
            return new SQLiteAsyncConnection(ruta);
        }
    }
}
