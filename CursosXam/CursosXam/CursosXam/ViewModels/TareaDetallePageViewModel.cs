﻿using CursosXam.AccesoDatos;
using CursosXam.Models;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursosXam.ViewModels
{
    public class TareaDetallePageViewModel
    {
        private ITareaAccesoDatos _tareaAccesoDatos;
        private IServiciosPagina _serviciosPagina;

        public Tarea Tarea { get; private set; }

        public ICommand GuardarCommand { get; private set; }

        public TareaDetallePageViewModel(TareaViewModel tareaViewModel, ITareaAccesoDatos tareaAccesoDatos, IServiciosPagina serviciosPagina)
        {
            if (tareaViewModel == null)
            {
                throw new ArgumentNullException(nameof(tareaViewModel));
            }

            _tareaAccesoDatos = tareaAccesoDatos;
            _serviciosPagina = serviciosPagina;

            GuardarCommand = new Command(async () => await Guardar());

            Tarea = new Tarea
            {
                Id = tareaViewModel.Id,
                Descripcion = tareaViewModel.Descripcion,
                Realizada = tareaViewModel.Realizada
            };
        }

        async Task Guardar()
        {
            if (String.IsNullOrWhiteSpace(Tarea.Descripcion))
            {
                await _serviciosPagina.DisplayAlert("Error", "Ingrese la descripción", "OK");
                return;
            }

            if (Tarea.Id == 0)
            {
                await _tareaAccesoDatos.AgregarTareaAsync(Tarea);
                MessagingCenter.Send(this, Eventos.AgregarTarea, Tarea);
            }
            else
            {
                await _tareaAccesoDatos.ActualizarTareaAsync(Tarea);
                MessagingCenter.Send(this, Eventos.ActualizarTarea, Tarea);
            }

            await _serviciosPagina.PopAsync();
        }
    }
}
