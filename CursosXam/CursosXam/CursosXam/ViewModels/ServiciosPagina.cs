﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CursosXam.ViewModels
{
    public class ServiciosPagina : IServiciosPagina
    {
        private Page MainPage
        {
            get { return Application.Current.MainPage; }
        }

        public async Task<bool> DisplayAlert(string titulo, string mensaje, string ok, string cancelar)
        {
            return await MainPage.DisplayAlert(titulo, mensaje, ok, cancelar);
        }

        public async Task DisplayAlert(string titulo, string mensaje, string ok)
        {
            await MainPage.DisplayAlert(titulo, mensaje, ok);
        }

        public async Task<Page> PopAsync()
        {
            return await MainPage.Navigation.PopAsync();
        }

        public async Task PushAsync(Page pagina)
        {
            await MainPage.Navigation.PushAsync(pagina);
        }
    }
}
