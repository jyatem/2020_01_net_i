﻿using CursosXam.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CursosXam.ViewModels
{
    public class TareaViewModel : BaseViewModel
    {
        public int Id { get; set; }

        public TareaViewModel()
        {
        }

        public TareaViewModel(Tarea tarea)
        {
            Id = tarea.Id;
            Descripcion = tarea.Descripcion;
            Realizada = tarea.Realizada;
        }

        private string _descripcion;

        public string Descripcion
        {
            get { return _descripcion; }
            set 
            {
                SetValue(ref _descripcion, value);
                OnPropertyChanged(nameof(Descripcion));
            }
        }

        private bool _realizada;

        public bool Realizada
        {
            get { return _realizada; }
            set 
            {
                SetValue(ref _realizada, value);
                OnPropertyChanged(nameof(RealizadaImagen));
            }
        }


        public ImageSource RealizadaImagen
        {
            get
            {
                if (Realizada)
                {
                    return ImageSource.FromFile("si.png");
                }

                return ImageSource.FromFile("no.png");
            }
        }

    }
}
