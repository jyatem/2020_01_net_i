﻿using CursosXam.AccesoDatos;
using CursosXam.Models;
using CursosXam.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CursosXam.ViewModels
{
    public class TareasPageViewModel : BaseViewModel
    {
        private TareaViewModel _tareaSeleccionada;
        private ITareaAccesoDatos _tareaAccesoDatos;
        private IServiciosPagina _serviciosPagina;

        private bool _datosCargos;

        public ObservableCollection<TareaViewModel> Tareas { get; private set; } = new ObservableCollection<TareaViewModel>();

        public TareaViewModel TareaSeleccionada
        {
            get { return _tareaSeleccionada; }
            set
            {
                SetValue(ref _tareaSeleccionada, value);
            }
        }

        public ICommand CargarDatosCommand { get; private set; }
        public ICommand AgregarTareaCommand { get; private set; }
        public ICommand SeleccionarTareaCommand { get; private set; }
        public ICommand EliminarTareaCommand { get; private set; }

        public TareasPageViewModel(ITareaAccesoDatos tareaAccesoDatos, IServiciosPagina serviciosPagina)
        {
            _tareaAccesoDatos = tareaAccesoDatos;
            _serviciosPagina = serviciosPagina;

            CargarDatosCommand = new Command(async () => await CargarDatos());
            AgregarTareaCommand = new Command(async () => await AgregarTarea());
            SeleccionarTareaCommand = new Command<TareaViewModel>(async t => await SeleccionarTarea(t));
            EliminarTareaCommand = new Command<TareaViewModel>(async t => await EliminarTarea(t));

            MessagingCenter.Subscribe<TareaDetallePageViewModel, Tarea>(this, Eventos.AgregarTarea, OnAgregarTarea);
            MessagingCenter.Subscribe<TareaDetallePageViewModel, Tarea>(this, Eventos.ActualizarTarea, OnActualizarTarea);
        }

        private void OnAgregarTarea(TareaDetallePageViewModel destino, Tarea tarea)
        {
            Tareas.Add(new TareaViewModel(tarea));
        }

        private void OnActualizarTarea(TareaDetallePageViewModel destino, Tarea tarea)
        {
            var tareaActualizarEnLaLista = Tareas.Single(t => t.Id == tarea.Id);

            tareaActualizarEnLaLista.Id = tarea.Id;
            tareaActualizarEnLaLista.Descripcion = tarea.Descripcion;
            tareaActualizarEnLaLista.Realizada = tarea.Realizada;
        }

        private async Task CargarDatos()
        {
            if (_datosCargos)
                return;

            _datosCargos = true;
            
            var tareas = await _tareaAccesoDatos.ObtenerTareasAsync();

            foreach (var tarea in tareas)
            {
                Tareas.Add(new TareaViewModel(tarea));
            }
        }

        private async Task AgregarTarea()
        {
            await _serviciosPagina.PushAsync(new TareaDetallePage(new TareaViewModel()));
        }

        private async Task SeleccionarTarea(TareaViewModel tareaViewModel)
        {
            if (tareaViewModel == null)
                return;

            TareaSeleccionada = null;
            await _serviciosPagina.PushAsync(new TareaDetallePage(tareaViewModel));
        }

        private async Task EliminarTarea(TareaViewModel tareaViewModel)
        {
            if (await _serviciosPagina.DisplayAlert("Advertencia", $"¿Está seguro de eliminar {tareaViewModel.Descripcion}", "Sí", "No"))
            {
                Tareas.Remove(tareaViewModel);

                var tarea = await _tareaAccesoDatos.ObtenerTareaAsync(tareaViewModel.Id);
                await _tareaAccesoDatos.EliminarTareaAsync(tarea);
            }
        }
    }
}
