﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CursosXam.ViewModels
{
    public interface IServiciosPagina
    {
        Task PushAsync(Page pagina);

        Task<Page> PopAsync();

        Task<bool> DisplayAlert(string titulo, string mensaje, string ok, string cancelar);

        Task DisplayAlert(string titulo, string mensaje, string ok);
    }
}
