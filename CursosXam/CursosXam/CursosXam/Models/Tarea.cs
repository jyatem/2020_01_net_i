﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursosXam.Models
{
    public class Tarea
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(255)]
        public string Descripcion { get; set; }

        public bool Realizada { get; set; }
    }
}
