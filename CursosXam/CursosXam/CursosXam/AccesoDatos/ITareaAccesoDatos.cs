﻿using CursosXam.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CursosXam.AccesoDatos
{
    public interface ITareaAccesoDatos
    {
        Task<IEnumerable<Tarea>> ObtenerTareasAsync();

        Task<Tarea> ObtenerTareaAsync(int id);

        Task AgregarTareaAsync(Tarea tarea);

        Task ActualizarTareaAsync(Tarea tarea);

        Task EliminarTareaAsync(Tarea tarea);
    }
}
