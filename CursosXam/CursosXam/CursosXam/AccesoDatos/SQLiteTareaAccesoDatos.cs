﻿using CursosXam.Models;
using SQLite;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CursosXam.AccesoDatos
{
    public class SQLiteTareaAccesoDatos : ITareaAccesoDatos
    {
        private SQLiteAsyncConnection _conexion;

        public SQLiteTareaAccesoDatos(ISQLiteDb db)
        {
            _conexion = db.ObtenerConexion();
            _conexion.CreateTableAsync<Tarea>();
        }

        public async Task ActualizarTareaAsync(Tarea tarea)
        {
            await _conexion.UpdateAsync(tarea);
        }

        public async Task AgregarTareaAsync(Tarea tarea)
        {
            await _conexion.InsertAsync(tarea);
        }

        public async Task EliminarTareaAsync(Tarea tarea)
        {
            await _conexion.DeleteAsync(tarea);
        }

        public async Task<Tarea> ObtenerTareaAsync(int id)
        {
            return await _conexion.FindAsync<Tarea>(id);
        }

        public async Task<IEnumerable<Tarea>> ObtenerTareasAsync()
        {
            return await _conexion.Table<Tarea>().ToListAsync();
        }
    }
}
