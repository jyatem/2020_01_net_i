﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace CursosXam.AccesoDatos
{
    public interface ISQLiteDb
    {
        SQLiteAsyncConnection ObtenerConexion();
    }
}
