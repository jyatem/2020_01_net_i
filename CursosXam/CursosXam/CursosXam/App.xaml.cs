﻿using CursosXam.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursosXam
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new TareasPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
