﻿using CursosXam.AccesoDatos;
using CursosXam.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursosXam.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TareaDetallePage : ContentPage
    {
        public TareaDetallePage(TareaViewModel tareaViewModel)
        {
            InitializeComponent();

            var tareaAccesoDatos = new SQLiteTareaAccesoDatos(DependencyService.Get<ISQLiteDb>());
            var serviciosPagina = new ServiciosPagina();

            Title = (tareaViewModel.Descripcion == null) ? "Nueva tarea" : "Editar tarea";
            BindingContext = new TareaDetallePageViewModel(tareaViewModel ?? new TareaViewModel(), tareaAccesoDatos, serviciosPagina);
        }
    }
}