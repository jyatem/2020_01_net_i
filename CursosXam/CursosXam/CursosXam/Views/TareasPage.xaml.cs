﻿using CursosXam.AccesoDatos;
using CursosXam.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CursosXam.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TareasPage : ContentPage
    {
        public TareasPageViewModel ViewModel
        {
            get { return BindingContext as TareasPageViewModel; }
            set { BindingContext = value; }
        }

        public TareasPage()
        {
            InitializeComponent();

            var tareaAccesoDatos = new SQLiteTareaAccesoDatos(DependencyService.Get<ISQLiteDb>());
            var serviciosPagina = new ServiciosPagina();

            ViewModel = new TareasPageViewModel(tareaAccesoDatos, serviciosPagina);
        }

        protected override void OnAppearing()
        {
            ViewModel.CargarDatosCommand.Execute(null);
            base.OnAppearing();
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ViewModel.SeleccionarTareaCommand.Execute(e.SelectedItem);
        }
    }
}