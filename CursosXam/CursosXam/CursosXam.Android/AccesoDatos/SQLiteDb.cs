﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CursosXam.AccesoDatos;
using CursosXam.Droid.AccesoDatos;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(SQLiteDb))]
namespace CursosXam.Droid.AccesoDatos
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteAsyncConnection ObtenerConexion()
        {
            var rutaCarpeta = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            var ruta = Path.Combine(rutaCarpeta, "CursosXam.db3");
            return new SQLiteAsyncConnection(ruta);
        }
    }
}