﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormValidadores.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormValidadores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table>
        <tr>
            <td colspan="2">
                <!-- Resumen de los mensajes de error -->
                <asp:ValidationSummary ID="vsErrores" runat="server" ForeColor="Red" DisplayMode="BulletList" HeaderText="Listado de errores" ShowSummary="false" ShowMessageBox="true" />
            </td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ForeColor="Red" ControlToValidate="txtNombre">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Apellidos:</td>
            <td>
                <asp:TextBox ID="txtApellido" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ErrorMessage="El apellido es requerido" ForeColor="Red" ControlToValidate="txtApellido">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Número de hijos:</td>
            <td>
                <asp:TextBox ID="txtNroHijos" runat="server" MaxLength="2" />
                <asp:RequiredFieldValidator ID="rfvNroHijos" runat="server" ErrorMessage="El número de hijos es requerido" ForeColor="Red" ControlToValidate="txtNroHijos">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvNroHijos" runat="server" ErrorMessage="El número de hijos debe ser un número entero" ForeColor="Red" ControlToValidate="txtNroHijos" Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                <asp:RangeValidator ID="rvNroHijos" runat="server" ErrorMessage="El número de hijos debe estar 0 y 10" ForeColor="Red" Type="Integer" ControlToValidate="txtNroHijos" MinimumValue="0" MaximumValue="10">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>Correo:</td>
            <td>
                <asp:TextBox ID="txtCorreo" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revCorreo" runat="server" ErrorMessage="El correo no es válido" ControlToValidate="txtCorreo" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Sólo letras:</td>
            <td>
                <asp:TextBox ID="txtLetras" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revSoloLetras" runat="server" ErrorMessage="Sólo letras" ControlToValidate="txtLetras" ForeColor="Red" ValidationExpression="^[A-Za-zÁÉÍÓÚáéíóúÑñ]+$">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Clave:</td>
            <td>
                <asp:TextBox ID="txtClave" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Confirmar clave:</td>
            <td>
                <asp:TextBox ID="txtConfirmarClave" runat="server" TextMode="Password"></asp:TextBox>
                <asp:CompareValidator ID="cvClave" runat="server" ErrorMessage="La clave no coincide" ControlToCompare="txtClave" ControlToValidate="txtConfirmarClave" ForeColor="Red">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>Número par:</td>
            <td>
                <asp:TextBox ID="txtEsPar" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="cvEsPar" runat="server" ErrorMessage="El número no es par" ControlToValidate="txtEsPar" ForeColor="Red" ClientValidationFunction="ValidarPar">*</asp:CustomValidator>
                <%--ValidationGroup="grupo1"--%>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" /><%--ValidationGroup="grupo1"--%>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CausesValidation="false"/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="lblMnensaje" runat="server"></asp:Label>
            </td>
        </tr>
    </table>

    <script>
        function ValidarPar(source, arguments) {
            var numero = document.getElementById('<%= txtEsPar.ClientID %>').value;

            if (isNaN(parseInt(numero))) {
                source.innerHTML = "No es número";
                arguments.IsValid = false;
                return;
            }

            numero = parseInt(numero);

            if (numero % 2 == 0) {
                arguments.IsValid = true;
            } else {
                arguments.IsValid = false;
            }
        }
    </script>

</asp:Content>
