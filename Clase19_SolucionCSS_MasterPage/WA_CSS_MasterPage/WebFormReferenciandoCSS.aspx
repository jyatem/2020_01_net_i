﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormReferenciandoCSS.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormReferenciandoCSS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Estilos/estilo.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <h1>Título con CSS referenciando</h1>
        <%--<p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
        </p>
        <p class="punteado">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
        </p>
        <p class="mayuscula">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
        </p>
        <p class="capitalize">
            jairo yesid yate martinez
        </p>--%>
        <%--<ul>
            <li>C#</li>
            <li>JavaScript</li>
            <li>Html</li>
            <li>CSS</li>
        </ul>
        <ul style="list-style: circle">
            <li>C#</li>
            <li>JavaScript</li>
            <li>Html</li>
            <li>CSS</li>
        </ul>
        <ul style="list-style: upper-roman">
            <li>C#</li>
            <li>JavaScript</li>
            <li>Html</li>
            <li>CSS</li>
        </ul>
        <ul style="list-style: lower-alpha">
            <li>C#</li>
            <li>JavaScript</li>
            <li>Html</li>
            <li>CSS</li>
        </ul>--%>
        <table>
            <tr>
                <th>Nombre</th>
                <th>Apellidos</th>
            </tr>
            <tr>
                <td>Jairo</td>
                <td>Yate</td>
            </tr>
            <tr>
                <td>Lina</td>
                <td>Mora</td>
            </tr>
        </table>
    </form>
</body>
</html>
