﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormEstilos.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormEstilos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <p>
            Parrafo sin estilo
        </p>
        <p style="color:#f5cc84; text-align:right">
            Parrafo con estilo
        </p>

    </form>
</body>
</html>
