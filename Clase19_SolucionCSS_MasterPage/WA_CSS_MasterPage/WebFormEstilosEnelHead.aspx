﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormEstilosEnelHead.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormEstilosEnelHead" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            background-color: #c4e5eb;
        }
        h1{
            text-align:center;
            color: #2693aa;
        }
        p {
            font-family: Verdana;
            font-size: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Página con CSS definido en el Head</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
        </p>
    </form>
</body>
</html>
