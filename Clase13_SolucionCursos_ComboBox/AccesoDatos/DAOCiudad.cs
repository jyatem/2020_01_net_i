﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAOCiudad
    {

        public List<Ciudad> RetornarCiudades()
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                SqlCommand comando = new SqlCommand();
                comando.Connection = conexion;
                comando.CommandText = "SELECT C.Id AS IdCiudad, NombreCiudad, IdDepartamento, NombreDepartamento FROM Ciudad AS C INNER JOIN Departamento AS D ON C.IdDepartamento = D.Id;";

                conexion.Open();

                SqlDataReader sqlDataReader = comando.ExecuteReader();

                List<Ciudad> ciudades = new List<Ciudad>();

                while (sqlDataReader.Read())
                {
                    // Si el dato a nivel de base de datos es nulo
                    //if (sqlDataReader["NombreCiudad"] == DBNull.Value)
                    //{
                    //}

                    Ciudad ciudad = new Ciudad
                    {
                        Id = Convert.ToInt32(sqlDataReader["IdCiudad"]),
                        NombreCiudad = sqlDataReader["NombreCiudad"].ToString(),
                        Departamento = new Departamento
                        {
                            Id = Convert.ToInt32(sqlDataReader["IdDepartamento"]),
                            NombreDepartamento = sqlDataReader["NombreDepartamento"].ToString()
                        }
                    };

                    ciudades.Add(ciudad);
                }

                sqlDataReader.Close();
                conexion.Close();

                return ciudades;
            }
        }

        public int ActualizarCiudad(Ciudad ciudad)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandText = "ActualizarCiudad";

                    SqlParameter sqlParameterId = new SqlParameter("@Id", SqlDbType.Int);
                    SqlParameter sqlParameterNombre = new SqlParameter("@NombreCiudad", SqlDbType.NVarChar, 1000);
                    SqlParameter sqlParameterIdDepartamento = new SqlParameter("@IdDepartamento", SqlDbType.Int);

                    sqlParameterId.Value = ciudad.Id;
                    sqlParameterNombre.Value = ciudad.NombreCiudad;
                    sqlParameterIdDepartamento.Value = ciudad.Departamento.Id;

                    comando.Parameters.Add(sqlParameterId);
                    comando.Parameters.Add(sqlParameterNombre);
                    comando.Parameters.Add(sqlParameterIdDepartamento);

                    conexion.Open();

                    int cantidadRegistrosActualizados = comando.ExecuteNonQuery();

                    conexion.Close();

                    return cantidadRegistrosActualizados;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
