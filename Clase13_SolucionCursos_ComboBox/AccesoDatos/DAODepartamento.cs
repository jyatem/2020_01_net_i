﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAODepartamento
    {

        public List<Departamento> RetornarDepartamentos()
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                SqlCommand comando = new SqlCommand();
                comando.Connection = conexion;
                comando.CommandText = "SELECT * FROM Departamento ORDER BY NombreDepartamento";

                conexion.Open();

                SqlDataReader sqlDataReader = comando.ExecuteReader();

                List<Departamento> departamentos = new List<Departamento>();

                //departamentos.Add(new Departamento { Id = 0, NombreDepartamento = "-- Seleccione un departamento --"});

                while (sqlDataReader.Read())
                {
                    Departamento departamento = new Departamento
                    {
                        Id = Convert.ToInt32(sqlDataReader["Id"]),
                        NombreDepartamento = sqlDataReader["NombreDepartamento"].ToString()
                    };

                    departamentos.Add(departamento);
                }

                sqlDataReader.Close();
                conexion.Close();
                
                return departamentos;
            }
        }
    }
}
