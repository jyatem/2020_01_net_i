﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCiudades : Form
    {
        private FachadaMaestras _fachadaMaestras;

        public FormCiudades()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        private void FormCiudades_Load(object sender, EventArgs e)
        {
            dgvCiudades.AutoGenerateColumns = false;
            //DataGridViewComboBoxColumn dcombo = null;
            //dcombo = dgvCiudades.Columns["cmbDepartamento"] as DataGridViewComboBoxColumn;
            //dcombo.DataPropertyName = "IdDepartamento";
            //dcombo.DisplayMember = "NombreDepartamento";
            //dcombo.ValueMember = "Id";

            cmbDepartamento.DataPropertyName = "IdDepartamento";
            cmbDepartamento.DisplayMember = "NombreDepartamento";
            cmbDepartamento.ValueMember = "Id";
            cmbDepartamento.DataSource = _fachadaMaestras.RetornarDepartamentos();
            dgvCiudades.DataSource = _fachadaMaestras.RetornarCiudades();            
        }
    }
}
