﻿namespace WF_Cursos
{
    partial class FormCiudades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCiudades = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreCiudad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreDepartamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbDepartamento = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCiudades)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCiudades
            // 
            this.dgvCiudades.AllowUserToDeleteRows = false;
            this.dgvCiudades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCiudades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.NombreCiudad,
            this.NombreDepartamento,
            this.cmbDepartamento});
            this.dgvCiudades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCiudades.Location = new System.Drawing.Point(0, 0);
            this.dgvCiudades.Name = "dgvCiudades";
            this.dgvCiudades.Size = new System.Drawing.Size(632, 212);
            this.dgvCiudades.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            // 
            // NombreCiudad
            // 
            this.NombreCiudad.DataPropertyName = "NombreCiudad";
            this.NombreCiudad.HeaderText = "Ciudad";
            this.NombreCiudad.Name = "NombreCiudad";
            // 
            // NombreDepartamento
            // 
            this.NombreDepartamento.DataPropertyName = "NombreDepartamento";
            this.NombreDepartamento.HeaderText = "Dpto";
            this.NombreDepartamento.Name = "NombreDepartamento";
            // 
            // cmbDepartamento
            // 
            this.cmbDepartamento.HeaderText = "CmbDpto";
            this.cmbDepartamento.Items.AddRange(new object[] {
            "Uno",
            "Dos",
            "Tres"});
            this.cmbDepartamento.Name = "cmbDepartamento";
            this.cmbDepartamento.Width = 150;
            // 
            // FormCiudades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 212);
            this.Controls.Add(this.dgvCiudades);
            this.Name = "FormCiudades";
            this.Text = "FormCiudades";
            this.Load += new System.EventHandler(this.FormCiudades_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCiudades)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCiudades;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreCiudad;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreDepartamento;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbDepartamento;
    }
}