﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Cursos
{
    class Program
    {
        static void Main(string[] args)
        {
			try
			{
				FachadaMaestras fachadaMaestras = new FachadaMaestras();

				List<Departamento> departamentos = fachadaMaestras.RetornarDepartamentos();

				foreach (Departamento departamento in departamentos)
				{
					Console.WriteLine($"Id: {departamento.Id} - Nombre: {departamento.NombreDepartamento}");
				}

				Console.WriteLine("----------------------------------------------");

				foreach (Ciudad ciudad in fachadaMaestras.RetornarCiudades())
				{
					Console.WriteLine($"IdCiudad {ciudad.Id} - Nombre Ciudad: {ciudad.NombreCiudad} - IdDepartamento: {ciudad.Departamento.Id} - Nombre Departamento: {ciudad.Departamento.NombreDepartamento}");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				Console.ReadLine();
			}
        }
    }
}
